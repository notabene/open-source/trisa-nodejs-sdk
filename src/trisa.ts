import { ChannelCredentials, ServiceError, credentials } from '@grpc/grpc-js';
import { encrypt } from '@richmonkeys/aes-256-gcm';
import { formatRFC3339 } from 'date-fns';

import {
  Payload,
  SecureEnvelope,
  ServiceState,
  SigningKey,
  TRISAHealthClient,
  TRISANetworkClient,
} from './protoc/trisa/api/v1beta1/api';

import {
  LookupReply,
  LookupRequest,
  SearchReply,
  TRISADirectoryClient,
} from './protoc/trisa/gds/api/v1beta1/api';
import {
  X509Certificate,
  constants,
  createHmac,
  createPublicKey,
  publicEncrypt,
  randomBytes,
  randomUUID,
} from 'crypto';

export type TrisaConfig = {
  clientCert: string;
  clientKey: string;
  rootCert: string;

  directoryAddress: string;
};

export type TransferRequest = {
  vaspId: string;
  address: string;
  asset: string;
  amount: number;
  ivms101: any;
};

export class Trisa {
  private rootCert;
  private clientCert;
  private clientKey;
  private directoryAddress;

  constructor(config: TrisaConfig) {
    this.rootCert = config.rootCert;
    this.clientCert = config.clientCert;
    this.clientKey = config.clientKey;
    this.directoryAddress = config.directoryAddress;
  }

  trasfer = async (transferRequest: TransferRequest): Promise<any> => {
    const { vaspId, address, asset, amount, ivms101 } = transferRequest;

    //Lookup entry endpoint
    const lookupResp = await this.trisaDirectoy_Lookup({
      id: vaspId,
      commonName: '',
      registeredDirectory: '',
    });
    const endpoint = lookupResp.endpoint;

    //Create identity
    const identity = ivms101;

    //Create transaction
    const transaction: any = {
      txid: '',
      originator: '',
      beneficiary: address,
      amount,
      network: '',
      timestamp: '',
      extra_json: '',
      asset_type: asset,
      tag: '',
    };

    //Create an Payload
    const payload: Payload = {
      identity,
      transaction,
      sentAt: '',
      receivedAt: '',
    };

    //Create encryption key
    const encryptionKey = randomBytes(256);

    //Encrypt payload
    const encryptedPayload = encrypt(JSON.stringify(payload), encryptionKey);

    //Create hmacSecret
    const hmacSecret = randomBytes(8).toString('hex');

    // Create HMAC
    const p_payload = Payload.encode(payload);
    const hmac = createHmac('sha256', hmacSecret)
      .update(p_payload.finish())
      .digest('hex');

    //Own certificate
    const cert = this.getCertificate();

    //Get public sealing key (we need to send ours)
    const receivedKey = await this.trisaNetwork_KeyExchange(endpoint, {
      version: 0,
      signature: Buffer.from(''),
      signatureAlgorithm: '',
      publicKeyAlgorithm: cert.publicKey.asymmetricKeyType as string,
      notBefore: cert.validFrom,
      notAfter: cert.validTo,
      revoked: false,
      data: cert.publicKey.export({ type: 'spki', format: 'der' }),
    });
    const js_receivedKey = createPublicKey({
      key: receivedKey.data,
      type: 'spki',
      format: 'der',
    });

    //Seal encryptionKey
    const sealedEncryptionKey = publicEncrypt(
      {
        key: js_receivedKey,
        padding: constants.RSA_PKCS1_OAEP_PADDING,
        oaepHash: 'sha512',
      },
      encryptionKey
    );

    //Seal hmacSecret
    const sealedHmacSecret = publicEncrypt(
      {
        key: js_receivedKey,
        padding: constants.RSA_PKCS1_OAEP_PADDING,
        oaepHash: 'sha512',
      },
      Buffer.from(hmacSecret)
    );

    //Create the secureEnvelope
    // https://trisa.dev/data/envelopes/#the-anatomy-of-a-secure-envelope
    const secureEnvelope: any = {
      //Envelope Metadata
      id: randomUUID(),
      timestamp: formatRFC3339(new Date(), { fractionDigits: 3 }),
      //Cryptographic Metadata
      encryptionKey: sealedEncryptionKey,
      encryptionAlgorithm: 'AES256-GCM',
      hmacSecret: sealedHmacSecret,
      hmacAlgorithm: 'HMAC-SHA256',
      sealed: true,
      publicKeySignature: Buffer.from(receivedKey.signature).toString(),
      //Payload
      payload: Buffer.from(encryptedPayload),
      hmac: Buffer.from(hmac, 'hex'),
    };

    //Sending Travel Rule info to
    const transferResp = await this.trisaNetwork_Transfer(
      endpoint,
      secureEnvelope
    );

    //TODO: Check what to respond
    return transferResp.toString();
  };

  getCertificate = (): X509Certificate => {
    const x509 = new X509Certificate(this.clientCert);
    return x509;
  };

  getChannelCredentials = (): ChannelCredentials => {
    const channelCredentials = ChannelCredentials.createSsl(
      Buffer.from(this.rootCert),
      Buffer.from(this.clientKey),
      Buffer.from(this.clientCert)
    );
    return channelCredentials;
  };

  /* TRISA API: https://trisa.dev/api/api/ */

  trisaHealth_Status = async (endpoint: string): Promise<ServiceState> => {
    return new Promise((resolve, reject) => {
      const client = new TRISAHealthClient(
        endpoint,
        this.getChannelCredentials()
      );
      client.status(
        {
          attempts: 0,
          lastCheckedAt: '',
        },
        (err: ServiceError | null, response: ServiceState) => {
          if (err) reject(err);
          resolve(response);
        }
      );
    });
  };

  trisaNetwork_KeyExchange = async (
    endpoint: string,
    signingKey: SigningKey
  ): Promise<SigningKey> => {
    return new Promise((resolve, reject) => {
      const client = new TRISANetworkClient(
        endpoint,
        this.getChannelCredentials()
      );
      client.keyExchange(
        signingKey,
        (err: ServiceError | null, response: SigningKey) => {
          if (err) reject(err);
          resolve(response);
        }
      );
    });
  };

  trisaNetwork_Transfer = async (
    endpoint: string,
    secureEnvelope: SecureEnvelope
  ): Promise<SecureEnvelope> => {
    return new Promise((resolve, reject) => {
      const client = new TRISANetworkClient(
        endpoint,
        this.getChannelCredentials()
      );
      client.transfer(
        secureEnvelope,
        (err: ServiceError | null, response: SecureEnvelope) => {
          if (err) reject(err);
          resolve(response);
        }
      );
    });
  };

  /* GDS API: https://trisa.dev/gds/ */

  trisaDirectoy_Lookup = async (
    lookupRequest: LookupRequest
  ): Promise<LookupReply> => {
    return new Promise((resolve, reject) => {
      const client = new TRISADirectoryClient(
        this.directoryAddress,
        credentials.createSsl()
      );
      client.lookup(
        lookupRequest,
        (err: ServiceError | null, response: LookupReply) => {
          if (err) reject(err.message);
          resolve(response);
        }
      );
    });
  };

  trisaDirectoy_Search = async (_cn: string): Promise<SearchReply> => {
    return new Promise((resolve, reject) => {
      const client = new TRISADirectoryClient(
        this.directoryAddress,
        credentials.createSsl()
      );
      client.search(
        {
          name: [_cn],
          website: [],
          country: [],
          businessCategory: [],
          vaspCategory: [],
        },
        (err: ServiceError | null, response: SearchReply) => {
          if (err) reject(err.message);
          resolve(response);
        }
      );
    });
  };

  trisaDirectoy_Status = async (): Promise<ServiceState> => {
    return new Promise((resolve, reject) => {
      const client = new TRISADirectoryClient(
        this.directoryAddress,
        credentials.createSsl()
      );
      client.status(
        {
          attempts: 0,
          lastCheckedAt: '',
        },
        (err: ServiceError | null, response: ServiceState) => {
          if (err) reject(err.message);
          resolve(response);
        }
      );
    });
  };
}
