import { describe, beforeAll, it, expect } from 'vitest';
import { Trisa } from '..';
import { caCert, clientCert, clientKey } from './constants';

describe('Trisa', () => {
  let trisa: Trisa;

  beforeAll(() => {
    trisa = new Trisa({
      clientCert: clientCert,
      clientKey: clientKey,
      rootCert: caCert,
      directoryAddress: 'api.trisatest.net:443',
    });
  });

  it('should get the status of alice rVASP ', async () => {
    const resp = await trisa.trisaHealth_Status('api.alice.vaspbot.net:443');
    //console.log(resp);
    expect(resp).not.toBeNull();
    expect(resp.status).toBe(1);
  });
});
