export const caCert = `-----BEGIN CERTIFICATE-----
MIIGqDCCBJCgAwIBAgIQM/F0Ep7Zers6ntNv6+LExTANBgkqhkiG9w0BAQwFADBv
MQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTETMBEGA1UEBxMKTWVu
bG8gUGFyazEYMBYGA1UEChMPQ2lwaGVyVHJhY2UgSW5jMRwwGgYDVQQDExNDaXBo
ZXJUcmFjZSBSb290IENBMB4XDTE5MDkyNjE0MTAzMloXDTM5MDkyNjE0MTAzMVow
cjELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExEzARBgNVBAcTCk1l
bmxvIFBhcmsxGDAWBgNVBAoTD0NpcGhlclRyYWNlIEluYzEfMB0GA1UEAxMWQ2lw
aGVyVHJhY2UgSXNzdWluZyBDQTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoC
ggIBAJ4KUZ1aecZ8hORB2l7D67mH754xFvu8UEdXJJMK8W+EmOE5U6YXKg4Vn9KJ
mcr4sCGm2Iz4m7OWekC2U03j4V3zgFPa9gXmRWS9VF5BQ6/qW/V3IOuZFDHG0nXf
HEjusROIfgTJxmsB5VWYiHIrwcPhP2GHZVcHxCRAP7Bck0QFIXQJ50K6LE9P+297
mM3epyIKo4mC1JD6aWgi7OgZ0oPRypFLw35+FeOE+MsPfaQ5IN9Fc3dSYRjmNqEI
p6HNKqMme0YYoZM1QYUykP6OFVPdxD1naa0AaTomk5HZ6XmiAxEBSRurdBJ3kNtH
dnJak8XVPgxNliQKW1Z1Qb4c6GtLWETc4oNKxaynw4Hc0E1PQoodUhBMRqqim6fa
G27LnJNGr7+WENwrBXDsizmdpwRy2EQkDnSh31Ykzfga8HHmHykmPYXlyjO4c8ct
8uPcjvZKRbLJ3WtUMGkBo6nud9l1n48VB/7azCyIrx++utm+MhzXbr+sMcVw+S9K
66WrKrQkmzaSze7IDkfQIvzcmuOQI1LoK37jFROWzQd9Hjv063HUedyS+XmhAI68
It+UONZ1WpJM7ECotHfCoYaJd2mnDdRY+qrrTJP78/kb2eO1367ooNIqAX2iZA29
+4ZcIgRM9eSjsCTD70XfU1zMv96C3xRiueyDFJZTyMc2SRl1AgMBAAGjggE7MIIB
NzAfBgNVHSMEGDAWgBTHMRl3RcgA6ebiLFe0g0QgoiEIhzAOBgNVHQ8BAf8EBAMC
AYYwEgYDVR0TAQH/BAgwBgEB/wIBADBJBgNVHR8EQjBAMD6gPKA6hjhodHRwOi8v
Y2lwaGVydHJhY2UuY3JsLnNlY3RpZ28uY29tL0NpcGhlclRyYWNlUm9vdENBLmNy
bDCBhQYIKwYBBQUHAQEEeTB3MEQGCCsGAQUFBzAChjhodHRwOi8vY2lwaGVydHJh
Y2UuY3J0LnNlY3RpZ28uY29tL0NpcGhlclRyYWNlUm9vdENBLmNydDAvBggrBgEF
BQcwAYYjaHR0cDovL2NpcGhlcnRyYWNlLm9jc3Auc2VjdGlnby5jb20wHQYDVR0O
BBYEFFu7iM7ZGDnLi2uC8QBvWmQm4/12MA0GCSqGSIb3DQEBDAUAA4ICAQBNvRki
qRCctQJd0llmYuT+Hk3UtzSHIFhYyKDiuolyHzl3gTzTLqk5xeqPtJ+zhmwERZRE
rYozG347AU3B0XckpmHBXZGyTSTKYyAW6whGJTLZ/8ytmFZTPynMJdTEYRSYRPLv
46WYgcq0tN7n19BSJWV02ktMg7KUbw4wlfJUHke86JcYlrmpSVnCiVNotnDRHhlT
NVIdW66RRGYgFO1gMwDnuiKpBkv8O7wqnfqlT5jezj2SkfT7u7q4HO1Lsendog1E
PsE38o29acaxvKXOopfqzaiJGf8jpCWASJS5V8wIFXtivUtDPDF4G9ZGrsTUelmq
Gms8IeiSALKfBhqynLDt4eH2qz7clABTK3boW0XoJc2a8euH0Z9p3b99M8kdzls7
GnD+5U971iCYVAT5XdoBQ8Fth0ojCVhnzzWpY7jybCPZiu/4Num8hJ0IKL79T1xJ
rbYeeBw5jEMzAy1d9jWxOr7Iutt21W25+88FGhQi3A10vMgp0FeJeFB1r5NfITk0
KDaPGLT1QbUnMNDeutebhjz883d8QJWNrCg++svK6QsRgvz777ADLfNe3wQnps+e
hf2VuDI97MKZTktkV1ZO+e1bEqTQFq15biXTXl8rKluoC8brPTJbXJt0FjKefTfT
/RdUKLiXC1tDgNoWccourK/UoV9PzH3AZf/Ruw==
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIFrDCCA5SgAwIBAgIQX/ZcgD1LlRvFxq1zf+x5BzANBgkqhkiG9w0BAQwFADBv
MQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTETMBEGA1UEBxMKTWVu
bG8gUGFyazEYMBYGA1UEChMPQ2lwaGVyVHJhY2UgSW5jMRwwGgYDVQQDExNDaXBo
ZXJUcmFjZSBSb290IENBMCAXDTE5MDkyNjE0MDMyMloYDzIwNTkwOTI2MTQwMzIx
WjBvMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTETMBEGA1UEBxMK
TWVubG8gUGFyazEYMBYGA1UEChMPQ2lwaGVyVHJhY2UgSW5jMRwwGgYDVQQDExND
aXBoZXJUcmFjZSBSb290IENBMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKC
AgEAyvukbc8yW9DF/aXYC3UwPgBBqc8s7hTCPM+vkEVQOjC9tl/x33y32eKNN6Lw
bqVub1Yuo3ZpjIb1Vt9D6iouukg2x7+9y3cZJ8t8cp4GGQCZSez15lHkwTxJgMok
ydOEh15SEr5AMZWpDG3bN1GcUQugnGdfFal2GwfxoBC9XT/q+RyF3kPhLNRy0xei
Lw96+bzI60fCJcgI0Wtb+wMJmsP4chl4Yrbfwe7AmZzQ44Si8FG3crSGItpVSGdM
n7K7jSX058lHaEWwrxE6t/6G0E+1Vb57pKAqf8qddyR09RMNARkBqggUUYCz2MUs
7atrS2CGEMef8x8LO/dAEqaTIRpgsSMQ8D9J/MYtPWdU/820nnMYoLgDpu4if1Hx
2od1h2N6N5i58j+qWtPACQPdcJx795DedhdS+FHVUA3qFfbwYNHC1vX4Pa2zQ6gj
dc4Y9KzwfB8nWYerUMuvb0HBib/7MvNErRextF6Qs3ouuqgeYhsNunSCGVSa62lX
iotX6mq83z502a88AacXI2iymVsJvbIaD+Wn0CR/jIs4EoEnyLuQs31fXmXi2EOu
Ra4szXMwswuZAphgLS6FK6t6bORozhYqbFDaZU6Um95KfzvPDaFrrFvdR33dayPU
bbLo4IUd5siUOcTtmV+pX/C2inv6Q833xz/2Xo2Sts2oMj0CAwEAAaNCMEAwDgYD
VR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFMcxGXdFyADp
5uIsV7SDRCCiIQiHMA0GCSqGSIb3DQEBDAUAA4ICAQA15r24A9g6eOERwF3CGmJ4
xUSJpRTR8SBIkCueKSKeRuxPOrGFUJUc5bZ9qflhiuCj5tVFP4RJqQBQrcnz5KgK
WpZWTALoZq9tKopUaqo/MGnKxsg8MwzkH8lbALG69a384Zm+P0wJ6L5dKWdkibF9
N9/oJ2IBMCdL6z9fvjw+GRI2EVUGMVPX0fycw1lrnaOVU2lOSIcWtsYXRiMgvY4M
qOdHRPYM0sd4dWMpIf/UxgHdhd66NmuijLIoL1mikt4NOifaC7mbvZrR0xMv2tUG
OKrbhLwGgbmEDc4ryboFQX1xNJOYDiS2uxy+4SNjib/gpMTsH603TusYegBE0gwB
Ntgwj4ki0OyBJRmegSBCExjO5aMxGF62V4fEixTADQdnBtNhJs889GkQwTR2VFrp
YqFphfc4//YQVDGPONdJMIy9Kt9IhRfjilvCq3gPRjt7Rys5+rT3Xsarv9YY5P6L
PPurieSi2wQce0zaFDJ2pZ2hlq9J6gLIs1rlhN3dSTqYlVCvfTYuLUlrXMqY2yNh
v+Nxj4a7JyUxD/dXEirlJWg4/eNbiXqVlzcAVuir5VSaFOBSFpW6Blfs/gvzKTN4
S5KedzefKOxbV+A0i8Livm3wRTkal1BFfQHbs/K2UIcHdnXb2QcXRf0xNB6mewJP
I/T63zeYXb9Ts08Buk7VZA==
-----END CERTIFICATE-----
`;

export const clientCert = `-----BEGIN CERTIFICATE-----
MIIHDjCCBPagAwIBAgIQUgprmXCi5cDa2X+xdUm3ajANBgkqhkiG9w0BAQwFADBy
MQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTETMBEGA1UEBxMKTWVu
bG8gUGFyazEYMBYGA1UEChMPQ2lwaGVyVHJhY2UgSW5jMR8wHQYDVQQDExZDaXBo
ZXJUcmFjZSBJc3N1aW5nIENBMB4XDTIzMDQxMTE0NTI1MFoXDTI0MDUxMTE0NTI0
OVowdTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExEzARBgNVBAcT
Ck1lbmxvIFBhcmsxGDAWBgNVBAoTD0NpcGhlclRyYWNlIEluYzEiMCAGA1UEAxMZ
dHJpc2EtdGVzdG5ldC5ub3RhYmVuZS5pZDCCAiIwDQYJKoZIhvcNAQEBBQADggIP
ADCCAgoCggIBAPknuy7i7vBqyIjGxR6q/xcw5F0r8kPGWQy5tpJ73n1YlTnlCLwX
PUvZqcfb62tYhb1tHgj45m6mJ4sccI3jju6rxuuOm4MAP1wwa5plid5Nfjfhjwi9
U198yP9RJBKXS0DK2kg6ehY1cIvBCwyPEmivXIkq/bH/E+222iy1bWoWosHTSzCE
Ilkr+yTglHI+aO5YRz0l/2y1bPIrZKdmnaGsUWIzoC+UsdYtGotrErzi++9Hyi09
Wk6OclLDUrN+EVeBO+TCo0N+nAxF/ABXPM9mk88o4qpRiKAhJF107bZzrS+rNDcT
yKUyLcw8BZRVwofJuz/KxhpJ7yLt22kfeqeLYLFICiIRHRqb4+p6QndsTL8SxcJU
EtKGoN6XGuui1hxmG/u0L8Py2IUoS9lyj7u0smMfNn9rf1eK+B4VnrKEGRn6mfqa
/gTn/3/CRJ9IDLdaa/GVqvcusA0x9DfyfyOAkbxBtgjN+hQ/J2yLpsa/xnKm8Mya
V55Z+NwOrrrvf3f9m0eBvwWM6NtZEi6IIHhGylEcS4Pyes/ekAvzvekdiofMPhoe
f0slkS0JCbi/b6tQSFdtCFZWW47p/GgR+fJdOUkrNnraFz/tSYHS97yadEeiEokS
J/2eIhTJidyf2D30cAiDqDPn1WQtdoPdTHtmPVcUGF+5oMmcttYdqL3RAgMBAAGj
ggGbMIIBlzAfBgNVHSMEGDAWgBRbu4jO2Rg5y4trgvEAb1pkJuP9djAOBgNVHQ8B
Af8EBAMCBeAwDAYDVR0TAQH/BAIwADAgBgNVHSUBAf8EFjAUBggrBgEFBQcDAgYI
KwYBBQUHAwEwVQYDVR0fBE4wTDBKoEigRoZEaHR0cDovL2NpcGhlcnRyYWNlLmNy
bC5pb3Quc2VjdGlnby5jb20vQ2lwaGVyVHJhY2VJbnRlcm1lZGlhdGVDQS5jcmww
gZcGCCsGAQUFBwEBBIGKMIGHMFAGCCsGAQUFBzAChkRodHRwOi8vY2lwaGVydHJh
Y2UuY3J0LmlvdC5zZWN0aWdvLmNvbS9DaXBoZXJUcmFjZUludGVybWVkaWF0ZUNB
LmNydDAzBggrBgEFBQcwAYYnaHR0cDovL2NpcGhlcnRyYWNlLm9jc3AuaW90LnNl
Y3RpZ28uY29tMCQGA1UdEQQdMBuCGXRyaXNhLXRlc3RuZXQubm90YWJlbmUuaWQw
HQYDVR0OBBYEFNf0deJWNkE5RcxLYrjUy5tq0zNyMA0GCSqGSIb3DQEBDAUAA4IC
AQBxCDJwBZoaXI22HFbk44QP/rWz3CZvBv6+nfmyoZKOZZcamRKws06sk3z7Ef6+
MJjrc4Xsy2pTzIuiUxeCeY7yDeV0xOH22reYjOoddv/ZTDr4YCCYVgbChGt20WI+
zNdvCW1kDpuoBj/vAT/7nla+It9qu/vk1r63hi/bbK4e4fn1RQFqNmn+ig5tvgxa
5CQxBYz4iXF8vdeOFpiYX4e/PqqJzp8USVSSf63wlNOm+IPlNNI7vDgC0KN3nwcM
fuNNR2z2Pb9A0HSpYNraircyghTv3CAHTwdDDrSfeAUmZl5TxjvjnZLPte72ZaRc
KiGwl6nEBMFZOlMa/TbJvuLpypUnwJEjmzseHUaXI3x7uoxZcrX6lVaaYFaJSOd2
eJdxMKBdimFPMtyK6Q93n2GeIpoFd3lOdzfKRttNzSjBSs07wVZ7AQxQv428AXJS
RaxqOw93S93ws7v/Tp0UKg5i/hoP5XtZyA8FT0h2xA5jrVkih2lHnm8tnSRBZdo+
rOZjZzM31F2AOXuAxkVlPQftk1yFBfUFUju7tdlDpOcEIFC/KGIf8limAiceW/Z0
Uy0Vn2xhNYSlAX+BVAtFuCha0uJdaG0TRSeh3gkt7XlrFBfKmKpaNIqP7TIz0m4j
K1PBPidVvYFU5PQuw0WfW2ZfO1Gd/omLM8DZGHcQh38wrA==
-----END CERTIFICATE-----
`;

export const clientKey = `-----BEGIN PRIVATE KEY-----
MIIJRAIBADANBgkqhkiG9w0BAQEFAASCCS4wggkqAgEAAoICAQD5J7su4u7wasiI
xsUeqv8XMORdK/JDxlkMubaSe959WJU55Qi8Fz1L2anH2+trWIW9bR4I+OZupieL
HHCN447uq8brjpuDAD9cMGuaZYneTX434Y8IvVNffMj/USQSl0tAytpIOnoWNXCL
wQsMjxJor1yJKv2x/xPtttostW1qFqLB00swhCJZK/sk4JRyPmjuWEc9Jf9stWzy
K2SnZp2hrFFiM6AvlLHWLRqLaxK84vvvR8otPVpOjnJSw1KzfhFXgTvkwqNDfpwM
RfwAVzzPZpPPKOKqUYigISRddO22c60vqzQ3E8ilMi3MPAWUVcKHybs/ysYaSe8i
7dtpH3qni2CxSAoiER0am+PqekJ3bEy/EsXCVBLShqDelxrrotYcZhv7tC/D8tiF
KEvZco+7tLJjHzZ/a39XivgeFZ6yhBkZ+pn6mv4E5/9/wkSfSAy3Wmvxlar3LrAN
MfQ38n8jgJG8QbYIzfoUPydsi6bGv8ZypvDMmleeWfjcDq667393/ZtHgb8FjOjb
WRIuiCB4RspRHEuD8nrP3pAL873pHYqHzD4aHn9LJZEtCQm4v2+rUEhXbQhWVluO
6fxoEfnyXTlJKzZ62hc/7UmB0ve8mnRHohKJEif9niIUyYncn9g99HAIg6gz59Vk
LXaD3Ux7Zj1XFBhfuaDJnLbWHai90QIDAQABAoICAHEVQFbSduI1hRjXJoZ509eB
wFQtawKp1qb4701i3SOE/g3Nzv4XiiGQBAgK37nT7rd/+/7UgTm9PftciBVOMARR
gLJPAnQdu6Qq8ductEtqXpwcSuALlfij2VJQUJADjQs2RoY7zEeiL5euoUYs9Yz/
8n7rPUP5YuEkebeLDML2zR4tuVWm0zQnyw/8wkNr4hIzjPnK8jgzy+PKIB/+ahMD
89gfWFfgJCryQ7b9fP6wAuqiabfIveEjs+2uay2+TT6rEDUOxi0PHOJRLUQ2VApK
I1IF24K0CM+s2yVzuaRysisNiGBrIUkhEjo9D8c4eVdKDyuv6+PopJn03P3EZNUb
IxQ8+yx2YBLptRRhEg3G+jhI4a+P0gUHNVWDrACQqG1Wrqi4yk4InbVUIGbT4V8y
kmf+V7oDNHNjAi+20gWJKoBxNGpQWQ6AsRdPWZZaiWsaaSzvpLt91airG3r4yJSl
U1kknjKRdFjbi8mSKqYmbSyKcJkv0qd/CRyDluCq8a9Zau2XgeqbBkFI1QeZYvGH
no5yr27MnGo/N2xBCPBoDQ9Mzj1mZfCmZpPiDw+37Ith963UWbb1ktNmPZYS1Udz
qT3oHRZWyUx3QDeHxsqDS2s0rZAZHnA7GX/C606GpDywRp9AP+uLU1RdJ4GzTSCB
194vTrClGglZUQTwwqhBAoIBAQD8fbG89WI8tknYe5NmeynoF6ISTVdXg951bPVB
dYVKgSxGRx7J1UGZ0mOB/VPcd82VOgPMFMuXyew0XYbrjdCb6Lr073a/0dYTlCZx
wjqLBixt0HRu4n8DulTxqJ9IGbIPhByEkP3SrYvjLUHq5HzImqUw27Ke5ydq+VRT
HA4x7XpmVzJWQ1Hf2EeIIfx6doSFZJVLY7vomKzsbIHLdoK0GRB8yqAknhQ8dhfJ
vH/jSOtPT0nJLx7XRNZKnKQB2CWt7wzqpXdiaWFRspRXEi8TFkiInbtpvfDAgUJu
f9wNtuMcOSOA2bYNoFlOhkIdQe3tjsLsdo7vZ5k3XQAWtTtJAoIBAQD8nis8558n
nPcL/qkLTlcEifH38Q0erZo2p7SeqLp8KSGIgyHbsBf2sD056z8SJ3+miQVjm1xX
tgRDVm3yokKbnd/5xewZl553eQZK4a/l1LSh/Jn8GzptuBWXLTBesouC8AN9A8tf
w2K08g7l9AXp0dSkc/cl6ehyfdEsqKmBiXe3UGpsBwo5MC2ClQPBZ90ZLJpruE3I
dq1XY24hc8ncJYCODM7SbLd2dyTn1Zo5rQIzVDKfMkHAvauKRWbIQ8rwKOORYaXs
tpyQwbiUdyepYW9NKE1dGlV9w0q47ToWo0i/1F8e9PtKMidCbj1n0U3NBbXob4ul
rtgvWcPVviZJAoIBAQC/ztTJEThRmsE6fZYaUhIhyGJMuQhS6VCDsAQYeCiL0tlJ
ghm3hhPRwOvrAgFFCJXaeToNPPb1FmYj2bD3HMKOzhvImmmBiuyv7unNnZlPp570
xVjOK64FrPV+p3RpF2wXMWtgoazQ8RnFuMnMoUrSMfivxxZIdJ2TPFl80WFPOHK1
cCRDN9zoyap9liPw/DvsAd8QaHGFXwZujHeFkz/zhN7rjhpd9uC0lZm4nUQh63Em
zLrm5K7vJdIM2DYXWJY3LdSIJK8uyZMutxTNmHi+bT0o23pV9/7+nrNSsjr/CCI7
fISmC72rWFa/umpbQ4x13O/jJqWq5O6WCkZY+dBRAoIBAQDSv9YMqqPBtZb5P8yh
rQq1s6vVkRnD9UsJu+HY16U0vk/GhrfxgriLSr7ZmiP1LlS1h+yiC9P/LM/jqbBy
UuJE13Iq++8b7rz6qVIRmLVZhZno5kdE5yZYsNH6XG3VIxl29GvEmSzLSMMJeEPr
nihsyGyhgkm3LKyLD+I5YsB/XEMvAPy4ASuLSGE/ZrhKWy8+uFJf+dOSoUe99yrC
58mJSxQz7gWB0cdBIub8LtzKr0Z9sgnFfHUUqmf5JCs6eixNLZ+lAxUdJb/6mFM4
MSw6iBoGAdCBEt4t3IZzPx5OEo031KJCvlkCrlLIfhXmjdxdZh07vAuB/xtGbZoO
sONZAoIBAQCJMF911gLRm3ZLJUMqZbo0wvmX7dAc8Sq0lqIoXQF0pFGIH/mpjR9M
hP6m4gT6NPWoDqtQkeLonZEREqb5IJmDX723Xm1O3OGhidF2VEImwwvpd9RKbV3l
bkX/q9xW8R+LXalicIz+KMoLqvjzLS81h3bnLfNRKkCfcgSIx9vp5vDTqM81DvVd
2bpF2/84u2S/4GoN4E5cs1wAix+A1zvRCbfcixitNkgkNIkZ0epmW6oM3Czqw8VZ
NIfTkefYoz5gM4NxesysuWZR6Mlgp3fIS2jP3y5Bi6D8kM0vwDincqRU6VYJMnfs
TP0mQyriAKwwW1+ffUNmSGo+wqCjyA9C
-----END PRIVATE KEY-----
`;
