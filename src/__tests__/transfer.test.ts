import { describe, beforeAll, it, expect } from 'vitest';
import { Trisa } from '..';
import { caCert, clientCert, clientKey } from './constants';

describe('Trisa', () => {
  let trisa: Trisa;

  beforeAll(() => {
    trisa = new Trisa({
      clientCert: clientCert,
      clientKey: clientKey,
      rootCert: caCert,
      directoryAddress: 'api.trisatest.net:443',
    });
  });

  it('should transfer to alice rVASP ', async () => {
    const transferRequest = {
      vaspId: '7a96ca2c-2818-4106-932e-1bcfd743b04c',
      address: '1ASkqdo1hvydosVRvRv2j6eNnWpWLHucMX',
      asset: 'BTC',
      amount: 1,
      ivms101: {
        originator: {
          originatorPersons: [
            {
              naturalPerson: {
                name: {
                  nameIdentifier: [
                    {
                      primaryIdentifier: 'Howard',
                      secondaryIdentifier: 'Jane',
                      nameIdentifierType: 'LEGL',
                    },
                    {
                      primaryIdentifier: 'Price',
                      secondaryIdentifier: 'Jane',
                      nameIdentifierType: 'MAID',
                    },
                  ],
                },
                geographicAddress: [
                  {
                    addressType: 'HOME',
                    streetName: 'Greystone Street',
                    buildingNumber: '28',
                    postCode: '38017',
                    townName: 'Collierville',
                    countrySubDivision: 'TN',
                    country: 'US',
                  },
                ],
                nationalIdentification: {
                  nationalIdentifier: '112502920',
                  nationalIdentifierType: 'SOCS',
                  countryOfIssue: 'US',
                  registrationAuthority: 'RA777777',
                },
                customerIdentification: '2642',
                dateAndPlaceOfBirth: {
                  dateOfBirth: '1992-10-04',
                  placeOfBirth: 'West Islip, NY',
                },
                countryOfResidence: 'US',
              },
            },
          ],
          accountNumber: ['14HmBSwec8XrcWge9Zi1ZngNia64u3Wd2v'],
        },
        beneficiary: {
          beneficiaryPersons: [
            {
              naturalPerson: {
                name: {
                  nameIdentifier: [
                    {
                      primaryIdentifier: 'Clark',
                      secondaryIdentifier: 'Lawrence',
                      nameIdentifierType: 'LEGL',
                    },
                    {
                      primaryIdentifier: 'Clark',
                      secondaryIdentifier: 'Larry',
                      nameIdentifierType: 'ALIA',
                    },
                  ],
                },
                geographicAddress: [
                  {
                    addressType: 'HOME',
                    streetName: 'Watling St',
                    buildingNumber: '249',
                    postCode: 'WD7 7AL',
                    townName: 'Radlett',
                    country: 'GB',
                  },
                ],
                nationalIdentification: {
                  nationalIdentifier: '319560446',
                  nationalIdentifierType: 'DRLC',
                  countryOfIssue: 'GB',
                  registrationAuthority: 'RA777777',
                },
                customerIdentification: '5610',
                dateAndPlaceOfBirth: {
                  dateOfBirth: '1986-12-13',
                  placeOfBirth: 'Leeds, United Kingdom',
                },
                countryOfResidence: 'GB',
              },
            },
          ],
          accountNumber: ['1ASkqdo1hvydosVRvRv2j6eNnWpWLHucMX'],
        },
      },
    };
    const resp = await trisa.trasfer(transferRequest);
    console.log(resp);
    expect(resp).not.toBeNull();
  });
});
