import { describe, beforeAll, it, expect } from 'vitest';
import { Trisa } from '..';
import { caCert, clientCert, clientKey } from './constants';

describe('Trisa', () => {
  let trisa: Trisa;

  beforeAll(() => {
    trisa = new Trisa({
      clientCert: clientCert,
      clientKey: clientKey,
      rootCert: caCert,
      directoryAddress: 'api.trisatest.net:443',
    });
  });

  it('should get the status of alice rVASP from the diretory', async () => {
    const resp = await trisa.trisaDirectoy_Lookup({
      id: '7a96ca2c-2818-4106-932e-1bcfd743b04c',
      registeredDirectory: '',
      commonName: '',
    });
    //console.log(resp);
    expect(resp).not.toBeNull();
    expect(resp.commonName).toBe('api.alice.vaspbot.net');
  });
});
