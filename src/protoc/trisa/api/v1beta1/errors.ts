/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { Any } from "../../../google/protobuf/any";

export const protobufPackage = "trisa.api.v1beta1";

export interface Error {
  /**
   * Error codes are standardized in the TRISA network to prevent confusion and to
   * allow easy identification of rejections or other problems so that the repair of
   * the connection or information exchange can be expedited.
   */
  code: Error_Code;
  /**
   * Human readable message stating the reason for the error, should be loggable and
   * actionable. Both standardized and unique/detail messages are acceptable.
   */
  message: string;
  /**
   * If the message that caused the error should be retried with a fix; otherwise the
   * rejection is permenant and the request should not be retried.
   */
  retry: boolean;
  /**
   * Any additional data or reasons for the rejection, e.g. a parent error, a diff,
   * a location for redirect, etc. The payload of the additional data should be
   * described by the error code.
   */
  details: Any | undefined;
}

export enum Error_Code {
  /**
   * UNHANDLED - Errors 0-49 are reserved for service-specific errors
   * Default error - something very bad happened.
   */
  UNHANDLED = 0,
  /** UNAVAILABLE - Generic error - could not handle request, retry later. */
  UNAVAILABLE = 1,
  /** SERVICE_DOWN_TIME - Alias: Sygna BVRC Rejected Type */
  SERVICE_DOWN_TIME = 1,
  /** BVRC002 - Alias: Sygna BVRC Rejected Code */
  BVRC002 = 1,
  /** MAINTENANCE - The service is currently in maintenance mode and cannot respond. */
  MAINTENANCE = 2,
  /** UNIMPLEMENTED - The RPC is not currently implemented by the TRISA node. */
  UNIMPLEMENTED = 3,
  /** INTERNAL_ERROR - Request could not be processed by recipient. */
  INTERNAL_ERROR = 49,
  /** BVRC999 - Alias: Sygna BVRC Rejected Code */
  BVRC999 = 49,
  /**
   * REJECTED - Errors 50-99 are reserved for transaction rejections.
   * Default rejection - no specified reason for rejecting the transaction.
   */
  REJECTED = 50,
  /** UNKNOWN_WALLET_ADDRESS - VASP does not control the specified wallet address. */
  UNKNOWN_WALLET_ADDRESS = 51,
  /** UNKNOWN_IDENTITY - VASP does not have KYC information for the specified wallet address. */
  UNKNOWN_IDENTITY = 52,
  /** UNKOWN_IDENTITY - Typo left for backwards compatibility. */
  UNKOWN_IDENTITY = 52,
  /** UNKNOWN_ORIGINATOR - Specifically, the Originator account cannot be identified. */
  UNKNOWN_ORIGINATOR = 53,
  /** UNKOWN_BENEFICIARY - Specifically, the Beneficiary account cannot be identified. */
  UNKOWN_BENEFICIARY = 54,
  /** BENEFICIARY_NAME_UNMATCHED - Alias: Sygna BVRC Rejected Type */
  BENEFICIARY_NAME_UNMATCHED = 54,
  /** BVRC007 - Alias: Sygna BVRC Rejected Code */
  BVRC007 = 54,
  /** UNSUPPORTED_CURRENCY - VASP cannot support the fiat currency or coin described in the transaction. */
  UNSUPPORTED_CURRENCY = 60,
  /** BVRC001 - Alias: Sygna BVRC Rejected Code */
  BVRC001 = 60,
  /** EXCEEDED_TRADING_VOLUME - No longer able to receive more transaction inflows */
  EXCEEDED_TRADING_VOLUME = 61,
  /** BVRC003 - Alias: Sygna BVRC Rejected Code */
  BVRC003 = 61,
  /** COMPLIANCE_CHECK_FAIL - An internal compliance check has failed or black listing has occurred */
  COMPLIANCE_CHECK_FAIL = 90,
  /** BVRC004 - Alias: Sygna BVRC Rejected Code */
  BVRC004 = 90,
  /** NO_COMPLIANCE - VASP not able to implement travel rule compliance. */
  NO_COMPLIANCE = 91,
  /** HIGH_RISK - VASP unwilling to conduct the transaction because of a risk assessment. */
  HIGH_RISK = 92,
  /** OUT_OF_NETWORK - Wallet address or transaction is not available on this network. */
  OUT_OF_NETWORK = 99,
  /**
   * FORBIDDEN - Errors 100-149 are reserved for authentication or cryptography failures.
   * Default access denied - no specified reason for forbidding the transaction.
   */
  FORBIDDEN = 100,
  /** NO_SIGNING_KEY - Could not sign transaction because no signing key is available. */
  NO_SIGNING_KEY = 101,
  /** CERTIFICATE_REVOKED - Could not sign transaction because keys have been revoked. */
  CERTIFICATE_REVOKED = 102,
  /** UNVERIFIED - Could not verify certificates with any certificate authority. */
  UNVERIFIED = 103,
  /** UNTRUSTED - A trusted connection could not be established. */
  UNTRUSTED = 104,
  /** INVALID_SIGNATURE - An HMAC signature could not be verified */
  INVALID_SIGNATURE = 105,
  /** INVALID_KEY - The transaction bundle cannot be decrypted with the specified key */
  INVALID_KEY = 106,
  /** ENVELOPE_DECODE_FAIL - Could not decode or decrypt private transaction data */
  ENVELOPE_DECODE_FAIL = 107,
  /** PRIVATE_INFO_DECODE_FAIL - Alias: Sygna BVRC Rejected Type */
  PRIVATE_INFO_DECODE_FAIL = 107,
  /** BVRC005 - Alias: Sygna BVRC Rejected Code */
  BVRC005 = 107,
  /** UNHANDLED_ALGORITHM - The algorithm specified by the encryption or signature is not implemented */
  UNHANDLED_ALGORITHM = 108,
  /**
   * BAD_REQUEST - Errors 150-199 are reserved for repairing exchange information.
   * Generic bad request - usually implies retry when reuqest is fixed.
   */
  BAD_REQUEST = 150,
  /** UNPARSEABLE_IDENTITY - Could not parse the identity record; specify the type in extra */
  UNPARSEABLE_IDENTITY = 151,
  /** PRIVATE_INFO_WRONG_FORMAT - Alias: Sygna BVRC Rejected Type */
  PRIVATE_INFO_WRONG_FORMAT = 151,
  /** BVRC006 - Alias: Sygna BVRC Rejected Code */
  BVRC006 = 151,
  /** UNPARSEABLE_TRANSACTION - Could not parse the transaction data record; specify the type in extra */
  UNPARSEABLE_TRANSACTION = 152,
  /**
   * MISSING_FIELDS - There are missing required fields in the transaction data, a list of these
   * fields is specified in extra.
   */
  MISSING_FIELDS = 153,
  /**
   * INCOMPLETE_IDENTITY - The identity record is not complete enough for compliance purposes of the
   * receiving VASPs. Required fields or format specified in extra.
   */
  INCOMPLETE_IDENTITY = 154,
  /** VALIDATION_ERROR - There was an error validating a field in the transaction data (specified in extra) */
  VALIDATION_ERROR = 155,
  UNRECOGNIZED = -1,
}

export function error_CodeFromJSON(object: any): Error_Code {
  switch (object) {
    case 0:
    case "UNHANDLED":
      return Error_Code.UNHANDLED;
    case 1:
    case "UNAVAILABLE":
      return Error_Code.UNAVAILABLE;
    case 1:
    case "SERVICE_DOWN_TIME":
      return Error_Code.SERVICE_DOWN_TIME;
    case 1:
    case "BVRC002":
      return Error_Code.BVRC002;
    case 2:
    case "MAINTENANCE":
      return Error_Code.MAINTENANCE;
    case 3:
    case "UNIMPLEMENTED":
      return Error_Code.UNIMPLEMENTED;
    case 49:
    case "INTERNAL_ERROR":
      return Error_Code.INTERNAL_ERROR;
    case 49:
    case "BVRC999":
      return Error_Code.BVRC999;
    case 50:
    case "REJECTED":
      return Error_Code.REJECTED;
    case 51:
    case "UNKNOWN_WALLET_ADDRESS":
      return Error_Code.UNKNOWN_WALLET_ADDRESS;
    case 52:
    case "UNKNOWN_IDENTITY":
      return Error_Code.UNKNOWN_IDENTITY;
    case 52:
    case "UNKOWN_IDENTITY":
      return Error_Code.UNKOWN_IDENTITY;
    case 53:
    case "UNKNOWN_ORIGINATOR":
      return Error_Code.UNKNOWN_ORIGINATOR;
    case 54:
    case "UNKOWN_BENEFICIARY":
      return Error_Code.UNKOWN_BENEFICIARY;
    case 54:
    case "BENEFICIARY_NAME_UNMATCHED":
      return Error_Code.BENEFICIARY_NAME_UNMATCHED;
    case 54:
    case "BVRC007":
      return Error_Code.BVRC007;
    case 60:
    case "UNSUPPORTED_CURRENCY":
      return Error_Code.UNSUPPORTED_CURRENCY;
    case 60:
    case "BVRC001":
      return Error_Code.BVRC001;
    case 61:
    case "EXCEEDED_TRADING_VOLUME":
      return Error_Code.EXCEEDED_TRADING_VOLUME;
    case 61:
    case "BVRC003":
      return Error_Code.BVRC003;
    case 90:
    case "COMPLIANCE_CHECK_FAIL":
      return Error_Code.COMPLIANCE_CHECK_FAIL;
    case 90:
    case "BVRC004":
      return Error_Code.BVRC004;
    case 91:
    case "NO_COMPLIANCE":
      return Error_Code.NO_COMPLIANCE;
    case 92:
    case "HIGH_RISK":
      return Error_Code.HIGH_RISK;
    case 99:
    case "OUT_OF_NETWORK":
      return Error_Code.OUT_OF_NETWORK;
    case 100:
    case "FORBIDDEN":
      return Error_Code.FORBIDDEN;
    case 101:
    case "NO_SIGNING_KEY":
      return Error_Code.NO_SIGNING_KEY;
    case 102:
    case "CERTIFICATE_REVOKED":
      return Error_Code.CERTIFICATE_REVOKED;
    case 103:
    case "UNVERIFIED":
      return Error_Code.UNVERIFIED;
    case 104:
    case "UNTRUSTED":
      return Error_Code.UNTRUSTED;
    case 105:
    case "INVALID_SIGNATURE":
      return Error_Code.INVALID_SIGNATURE;
    case 106:
    case "INVALID_KEY":
      return Error_Code.INVALID_KEY;
    case 107:
    case "ENVELOPE_DECODE_FAIL":
      return Error_Code.ENVELOPE_DECODE_FAIL;
    case 107:
    case "PRIVATE_INFO_DECODE_FAIL":
      return Error_Code.PRIVATE_INFO_DECODE_FAIL;
    case 107:
    case "BVRC005":
      return Error_Code.BVRC005;
    case 108:
    case "UNHANDLED_ALGORITHM":
      return Error_Code.UNHANDLED_ALGORITHM;
    case 150:
    case "BAD_REQUEST":
      return Error_Code.BAD_REQUEST;
    case 151:
    case "UNPARSEABLE_IDENTITY":
      return Error_Code.UNPARSEABLE_IDENTITY;
    case 151:
    case "PRIVATE_INFO_WRONG_FORMAT":
      return Error_Code.PRIVATE_INFO_WRONG_FORMAT;
    case 151:
    case "BVRC006":
      return Error_Code.BVRC006;
    case 152:
    case "UNPARSEABLE_TRANSACTION":
      return Error_Code.UNPARSEABLE_TRANSACTION;
    case 153:
    case "MISSING_FIELDS":
      return Error_Code.MISSING_FIELDS;
    case 154:
    case "INCOMPLETE_IDENTITY":
      return Error_Code.INCOMPLETE_IDENTITY;
    case 155:
    case "VALIDATION_ERROR":
      return Error_Code.VALIDATION_ERROR;
    case -1:
    case "UNRECOGNIZED":
    default:
      return Error_Code.UNRECOGNIZED;
  }
}

export function error_CodeToJSON(object: Error_Code): string {
  switch (object) {
    case Error_Code.UNHANDLED:
      return "UNHANDLED";
    case Error_Code.UNAVAILABLE:
      return "UNAVAILABLE";
    case Error_Code.SERVICE_DOWN_TIME:
      return "SERVICE_DOWN_TIME";
    case Error_Code.BVRC002:
      return "BVRC002";
    case Error_Code.MAINTENANCE:
      return "MAINTENANCE";
    case Error_Code.UNIMPLEMENTED:
      return "UNIMPLEMENTED";
    case Error_Code.INTERNAL_ERROR:
      return "INTERNAL_ERROR";
    case Error_Code.BVRC999:
      return "BVRC999";
    case Error_Code.REJECTED:
      return "REJECTED";
    case Error_Code.UNKNOWN_WALLET_ADDRESS:
      return "UNKNOWN_WALLET_ADDRESS";
    case Error_Code.UNKNOWN_IDENTITY:
      return "UNKNOWN_IDENTITY";
    case Error_Code.UNKOWN_IDENTITY:
      return "UNKOWN_IDENTITY";
    case Error_Code.UNKNOWN_ORIGINATOR:
      return "UNKNOWN_ORIGINATOR";
    case Error_Code.UNKOWN_BENEFICIARY:
      return "UNKOWN_BENEFICIARY";
    case Error_Code.BENEFICIARY_NAME_UNMATCHED:
      return "BENEFICIARY_NAME_UNMATCHED";
    case Error_Code.BVRC007:
      return "BVRC007";
    case Error_Code.UNSUPPORTED_CURRENCY:
      return "UNSUPPORTED_CURRENCY";
    case Error_Code.BVRC001:
      return "BVRC001";
    case Error_Code.EXCEEDED_TRADING_VOLUME:
      return "EXCEEDED_TRADING_VOLUME";
    case Error_Code.BVRC003:
      return "BVRC003";
    case Error_Code.COMPLIANCE_CHECK_FAIL:
      return "COMPLIANCE_CHECK_FAIL";
    case Error_Code.BVRC004:
      return "BVRC004";
    case Error_Code.NO_COMPLIANCE:
      return "NO_COMPLIANCE";
    case Error_Code.HIGH_RISK:
      return "HIGH_RISK";
    case Error_Code.OUT_OF_NETWORK:
      return "OUT_OF_NETWORK";
    case Error_Code.FORBIDDEN:
      return "FORBIDDEN";
    case Error_Code.NO_SIGNING_KEY:
      return "NO_SIGNING_KEY";
    case Error_Code.CERTIFICATE_REVOKED:
      return "CERTIFICATE_REVOKED";
    case Error_Code.UNVERIFIED:
      return "UNVERIFIED";
    case Error_Code.UNTRUSTED:
      return "UNTRUSTED";
    case Error_Code.INVALID_SIGNATURE:
      return "INVALID_SIGNATURE";
    case Error_Code.INVALID_KEY:
      return "INVALID_KEY";
    case Error_Code.ENVELOPE_DECODE_FAIL:
      return "ENVELOPE_DECODE_FAIL";
    case Error_Code.PRIVATE_INFO_DECODE_FAIL:
      return "PRIVATE_INFO_DECODE_FAIL";
    case Error_Code.BVRC005:
      return "BVRC005";
    case Error_Code.UNHANDLED_ALGORITHM:
      return "UNHANDLED_ALGORITHM";
    case Error_Code.BAD_REQUEST:
      return "BAD_REQUEST";
    case Error_Code.UNPARSEABLE_IDENTITY:
      return "UNPARSEABLE_IDENTITY";
    case Error_Code.PRIVATE_INFO_WRONG_FORMAT:
      return "PRIVATE_INFO_WRONG_FORMAT";
    case Error_Code.BVRC006:
      return "BVRC006";
    case Error_Code.UNPARSEABLE_TRANSACTION:
      return "UNPARSEABLE_TRANSACTION";
    case Error_Code.MISSING_FIELDS:
      return "MISSING_FIELDS";
    case Error_Code.INCOMPLETE_IDENTITY:
      return "INCOMPLETE_IDENTITY";
    case Error_Code.VALIDATION_ERROR:
      return "VALIDATION_ERROR";
    case Error_Code.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

function createBaseError(): Error {
  return { code: 0, message: "", retry: false, details: undefined };
}

export const Error = {
  encode(message: Error, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.code !== 0) {
      writer.uint32(8).int32(message.code);
    }
    if (message.message !== "") {
      writer.uint32(18).string(message.message);
    }
    if (message.retry === true) {
      writer.uint32(24).bool(message.retry);
    }
    if (message.details !== undefined) {
      Any.encode(message.details, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Error {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseError();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.code = reader.int32() as any;
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.message = reader.string();
          continue;
        case 3:
          if (tag != 24) {
            break;
          }

          message.retry = reader.bool();
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.details = Any.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Error {
    return {
      code: isSet(object.code) ? error_CodeFromJSON(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : "",
      retry: isSet(object.retry) ? Boolean(object.retry) : false,
      details: isSet(object.details) ? Any.fromJSON(object.details) : undefined,
    };
  },

  toJSON(message: Error): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = error_CodeToJSON(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.retry !== undefined && (obj.retry = message.retry);
    message.details !== undefined && (obj.details = message.details ? Any.toJSON(message.details) : undefined);
    return obj;
  },

  create<I extends Exact<DeepPartial<Error>, I>>(base?: I): Error {
    return Error.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Error>, I>>(object: I): Error {
    const message = createBaseError();
    message.code = object.code ?? 0;
    message.message = object.message ?? "";
    message.retry = object.retry ?? false;
    message.details = (object.details !== undefined && object.details !== null)
      ? Any.fromPartial(object.details)
      : undefined;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
