/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Any } from "../../../../google/protobuf/any";
import { LegalPerson, NaturalPerson } from "../../../../ivms101/ivms101";
import { Certificate } from "./ca";

export const protobufPackage = "trisa.gds.models.v1beta1";

export enum BusinessCategory {
  UNKNOWN_ENTITY = 0,
  PRIVATE_ORGANIZATION = 1,
  GOVERNMENT_ENTITY = 2,
  BUSINESS_ENTITY = 3,
  NON_COMMERCIAL_ENTITY = 4,
  UNRECOGNIZED = -1,
}

export function businessCategoryFromJSON(object: any): BusinessCategory {
  switch (object) {
    case 0:
    case "UNKNOWN_ENTITY":
      return BusinessCategory.UNKNOWN_ENTITY;
    case 1:
    case "PRIVATE_ORGANIZATION":
      return BusinessCategory.PRIVATE_ORGANIZATION;
    case 2:
    case "GOVERNMENT_ENTITY":
      return BusinessCategory.GOVERNMENT_ENTITY;
    case 3:
    case "BUSINESS_ENTITY":
      return BusinessCategory.BUSINESS_ENTITY;
    case 4:
    case "NON_COMMERCIAL_ENTITY":
      return BusinessCategory.NON_COMMERCIAL_ENTITY;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BusinessCategory.UNRECOGNIZED;
  }
}

export function businessCategoryToJSON(object: BusinessCategory): string {
  switch (object) {
    case BusinessCategory.UNKNOWN_ENTITY:
      return "UNKNOWN_ENTITY";
    case BusinessCategory.PRIVATE_ORGANIZATION:
      return "PRIVATE_ORGANIZATION";
    case BusinessCategory.GOVERNMENT_ENTITY:
      return "GOVERNMENT_ENTITY";
    case BusinessCategory.BUSINESS_ENTITY:
      return "BUSINESS_ENTITY";
    case BusinessCategory.NON_COMMERCIAL_ENTITY:
      return "NON_COMMERCIAL_ENTITY";
    case BusinessCategory.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum VerificationState {
  NO_VERIFICATION = 0,
  SUBMITTED = 1,
  EMAIL_VERIFIED = 2,
  PENDING_REVIEW = 3,
  REVIEWED = 4,
  ISSUING_CERTIFICATE = 5,
  VERIFIED = 6,
  REJECTED = 7,
  APPEALED = 8,
  ERRORED = 9,
  UNRECOGNIZED = -1,
}

export function verificationStateFromJSON(object: any): VerificationState {
  switch (object) {
    case 0:
    case "NO_VERIFICATION":
      return VerificationState.NO_VERIFICATION;
    case 1:
    case "SUBMITTED":
      return VerificationState.SUBMITTED;
    case 2:
    case "EMAIL_VERIFIED":
      return VerificationState.EMAIL_VERIFIED;
    case 3:
    case "PENDING_REVIEW":
      return VerificationState.PENDING_REVIEW;
    case 4:
    case "REVIEWED":
      return VerificationState.REVIEWED;
    case 5:
    case "ISSUING_CERTIFICATE":
      return VerificationState.ISSUING_CERTIFICATE;
    case 6:
    case "VERIFIED":
      return VerificationState.VERIFIED;
    case 7:
    case "REJECTED":
      return VerificationState.REJECTED;
    case 8:
    case "APPEALED":
      return VerificationState.APPEALED;
    case 9:
    case "ERRORED":
      return VerificationState.ERRORED;
    case -1:
    case "UNRECOGNIZED":
    default:
      return VerificationState.UNRECOGNIZED;
  }
}

export function verificationStateToJSON(object: VerificationState): string {
  switch (object) {
    case VerificationState.NO_VERIFICATION:
      return "NO_VERIFICATION";
    case VerificationState.SUBMITTED:
      return "SUBMITTED";
    case VerificationState.EMAIL_VERIFIED:
      return "EMAIL_VERIFIED";
    case VerificationState.PENDING_REVIEW:
      return "PENDING_REVIEW";
    case VerificationState.REVIEWED:
      return "REVIEWED";
    case VerificationState.ISSUING_CERTIFICATE:
      return "ISSUING_CERTIFICATE";
    case VerificationState.VERIFIED:
      return "VERIFIED";
    case VerificationState.REJECTED:
      return "REJECTED";
    case VerificationState.APPEALED:
      return "APPEALED";
    case VerificationState.ERRORED:
      return "ERRORED";
    case VerificationState.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum ServiceState {
  UNKNOWN = 0,
  HEALTHY = 1,
  UNHEALTHY = 2,
  DANGER = 3,
  OFFLINE = 4,
  MAINTENANCE = 5,
  UNRECOGNIZED = -1,
}

export function serviceStateFromJSON(object: any): ServiceState {
  switch (object) {
    case 0:
    case "UNKNOWN":
      return ServiceState.UNKNOWN;
    case 1:
    case "HEALTHY":
      return ServiceState.HEALTHY;
    case 2:
    case "UNHEALTHY":
      return ServiceState.UNHEALTHY;
    case 3:
    case "DANGER":
      return ServiceState.DANGER;
    case 4:
    case "OFFLINE":
      return ServiceState.OFFLINE;
    case 5:
    case "MAINTENANCE":
      return ServiceState.MAINTENANCE;
    case -1:
    case "UNRECOGNIZED":
    default:
      return ServiceState.UNRECOGNIZED;
  }
}

export function serviceStateToJSON(object: ServiceState): string {
  switch (object) {
    case ServiceState.UNKNOWN:
      return "UNKNOWN";
    case ServiceState.HEALTHY:
      return "HEALTHY";
    case ServiceState.UNHEALTHY:
      return "UNHEALTHY";
    case ServiceState.DANGER:
      return "DANGER";
    case ServiceState.OFFLINE:
      return "OFFLINE";
    case ServiceState.MAINTENANCE:
      return "MAINTENANCE";
    case ServiceState.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

/**
 * VASP represents the top-level directory entry for certificate public key exchange.
 * The TRISA Directory service allows search and lookup of VASP entries and returns
 * TRISA implementation details and certificate key material. VASPs must be registered
 * with IVMS 101 identity data for the business entity as well as natural person
 * entities for technical, legal, billing, and administrative contacts.
 *
 * A VASP entry is also the primary point of replication between directories that
 * implement the directory replication service. It maintains the version information to
 * detect changes with respect to a specific registered directory and faciliates
 * anti-entropy gossip protocols.
 */
export interface VASP {
  /**
   * A unique identifier generated by the directory service, should be a globally
   * unique identifier generated by the directory specified in registered_directory.
   */
  id: string;
  /**
   * The url of directory that registered this VASP, e.g. vaspdirectory.net. The
   * id of the VASP must must be unique with respect to this field.
   */
  registeredDirectory: string;
  /**
   * The legal entity IVMS 101 data for VASP KYC information exchange. This is the
   * IVMS 101 data that should be exchanged in the TRISA P2P protocol as the
   * Originator, Intermediate, or Beneficiary VASP fields. A complete and valid
   * identity record with country of registration is required.
   */
  entity:
    | LegalPerson
    | undefined;
  /** Technical, legal, billing, and administrative contacts for the VASP. */
  contacts:
    | Contacts
    | undefined;
  /**
   * Certificate information and public key material issued to the VASP to facilitate
   * mTLS connections between TRISA partners. If the VASP has not been verified then
   * the certificate will not be issued. This field is the most recently issued
   * certificate but may or may not be revoked.
   * In the white paper, this is referred to as the Identity EV-Cert.
   */
  identityCertificate:
    | Certificate
    | undefined;
  /**
   * Signing-key certificates and public key material used to sign transactions. The
   * primary use of signing-key certificates is to asymmetrically encrypt and sign
   * per-transaction symmetric encryption keys. A VASP can maintain any number of
   * signing certificates, which are idnetified by their signature or serial number.
   */
  signingCertificates: Certificate[];
  /**
   * Domain name of the TRISA endpoint used as the common name for the certificate.
   * This field must be unique per VASP as it identifies the Certificate and is used
   * directly in lookups.
   */
  commonName: string;
  /**
   * Travel Rule Implementation Endpoint - where other TRISA peers should connect.
   * This should be an addr:port combination, e.g. trisa.vaspbot.net:443
   */
  trisaEndpoint: string;
  /** Business Information */
  website: string;
  businessCategory: BusinessCategory;
  vaspCategories: string[];
  /** Should be a date in YYYY-MM-DD format */
  establishedOn: string;
  /** TRIXO Questionnaire */
  trixo:
    | TRIXOQuestionnaire
    | undefined;
  /**
   * Directory Service Metadata
   * Should not be populated by client code.
   */
  verificationStatus: VerificationState;
  serviceStatus: ServiceState;
  /** Should be an RFC 3339 Timestamp */
  verifiedOn: string;
  /** Should be an RFC 3339 Timestamp */
  firstListed: string;
  /** Should be an RFC 3339 Timestamp */
  lastUpdated: string;
  /**
   * The legal entity signature that is used to verify uniqueness or detect changes.
   * It is used primarily by the directory service because the hash of a VASP object
   * is not specified in the whitepaper.
   * Should not be populated by client code.
   */
  signature: Buffer;
  /**
   * Version is used for anti-entropy based replication. It is used primarily by the
   * directory service and is not specified in the TRISA whitepaper.
   * Should not be populated by client code.
   */
  version:
    | Version
    | undefined;
  /**
   * Extra data that might be stored by the directory service related to the VASP.
   * Should not be populated by client code.
   */
  extra: Any | undefined;
}

/**
 * At least one of the following contact information is required for the VASP to be
 * registered in a TRISA directory. Contact information should be kept private in the
 * directory service and only used for email communication or verification.
 */
export interface Contacts {
  technical: Contact | undefined;
  administrative: Contact | undefined;
  legal: Contact | undefined;
  billing: Contact | undefined;
}

export interface Contact {
  /** Name is required to identify and address the contact */
  name: string;
  /** An email address is required for all contacts */
  email: string;
  /** Phone number is optional, but it is strongly suggested */
  phone: string;
  /** Optional KYC data if required for the directory service contact. */
  person:
    | NaturalPerson
    | undefined;
  /**
   * Extra data that might be stored by the directory service related to the Contact.
   * Should not be populated by client code.
   */
  extra: Any | undefined;
}

export interface TRIXOQuestionnaire {
  /** Should be the name of the country or an ISO-3166-1 code. */
  primaryNationalJurisdiction: string;
  /** Name of primary financial regulator or supervisory authority. */
  primaryRegulator: string;
  /**
   * Is the VASP permitted to send and/or receive transfers of virtual assets in the
   * jurisdictions in which it operates?
   * One of yes, no, partially
   */
  financialTransfersPermitted: string;
  /** Other jurisdictions in which the entity operates. */
  otherJurisdictions: Jurisdiction[];
  /**
   * Does the VASP have a programme that sets minimum AML, CFT, KYC/CDD and sanctions
   * standards per the requirements of the jurisdiction(s) regulatory regimes where
   * it is licensed/approved/registered?
   * Either yes or no
   */
  hasRequiredRegulatoryProgram: string;
  /**
   * Does the VASP conduct KYC/CDD before permitting its customers to send/receive
   * virtual asset transfers?
   */
  conductsCustomerKyc: boolean;
  /** At what threshold does the VASP conduct KYC? */
  kycThreshold: number;
  kycThresholdCurrency: string;
  /**
   * Is the VASP required to comply with the application of the Travel Rule standards
   * in the jurisdiction(s) where it is licensed/approved/registered?
   */
  mustComplyTravelRule: boolean;
  /** Applicable Travel Regulations the VASP must comply with. */
  applicableRegulations: string[];
  /** What is the minimum threshold for travel rule compliance? */
  complianceThreshold: number;
  complianceThresholdCurrency: string;
  /** Is the VASP required by law to safeguard PII? */
  mustSafeguardPii: boolean;
  /**
   * Does the VASP secure and protect PII, including PII received from other VASPs
   * under the Travel Rule? (yes/no)
   */
  safeguardsPii: boolean;
}

export interface Jurisdiction {
  country: string;
  regulatorName: string;
  licenseNumber: string;
}

/** Implements a distributed version as a Lamport Scalar */
export interface Version {
  /** Process ID - used to deconflict ties in the version number. */
  pid: number;
  /** Montonically increasing version number. */
  version: number;
}

function createBaseVASP(): VASP {
  return {
    id: "",
    registeredDirectory: "",
    entity: undefined,
    contacts: undefined,
    identityCertificate: undefined,
    signingCertificates: [],
    commonName: "",
    trisaEndpoint: "",
    website: "",
    businessCategory: 0,
    vaspCategories: [],
    establishedOn: "",
    trixo: undefined,
    verificationStatus: 0,
    serviceStatus: 0,
    verifiedOn: "",
    firstListed: "",
    lastUpdated: "",
    signature: Buffer.alloc(0),
    version: undefined,
    extra: undefined,
  };
}

export const VASP = {
  encode(message: VASP, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    if (message.registeredDirectory !== "") {
      writer.uint32(18).string(message.registeredDirectory);
    }
    if (message.entity !== undefined) {
      LegalPerson.encode(message.entity, writer.uint32(26).fork()).ldelim();
    }
    if (message.contacts !== undefined) {
      Contacts.encode(message.contacts, writer.uint32(34).fork()).ldelim();
    }
    if (message.identityCertificate !== undefined) {
      Certificate.encode(message.identityCertificate, writer.uint32(42).fork()).ldelim();
    }
    for (const v of message.signingCertificates) {
      Certificate.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    if (message.commonName !== "") {
      writer.uint32(58).string(message.commonName);
    }
    if (message.trisaEndpoint !== "") {
      writer.uint32(66).string(message.trisaEndpoint);
    }
    if (message.website !== "") {
      writer.uint32(74).string(message.website);
    }
    if (message.businessCategory !== 0) {
      writer.uint32(80).int32(message.businessCategory);
    }
    for (const v of message.vaspCategories) {
      writer.uint32(90).string(v!);
    }
    if (message.establishedOn !== "") {
      writer.uint32(98).string(message.establishedOn);
    }
    if (message.trixo !== undefined) {
      TRIXOQuestionnaire.encode(message.trixo, writer.uint32(106).fork()).ldelim();
    }
    if (message.verificationStatus !== 0) {
      writer.uint32(112).int32(message.verificationStatus);
    }
    if (message.serviceStatus !== 0) {
      writer.uint32(120).int32(message.serviceStatus);
    }
    if (message.verifiedOn !== "") {
      writer.uint32(130).string(message.verifiedOn);
    }
    if (message.firstListed !== "") {
      writer.uint32(138).string(message.firstListed);
    }
    if (message.lastUpdated !== "") {
      writer.uint32(146).string(message.lastUpdated);
    }
    if (message.signature.length !== 0) {
      writer.uint32(154).bytes(message.signature);
    }
    if (message.version !== undefined) {
      Version.encode(message.version, writer.uint32(162).fork()).ldelim();
    }
    if (message.extra !== undefined) {
      Any.encode(message.extra, writer.uint32(170).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): VASP {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseVASP();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.id = reader.string();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.registeredDirectory = reader.string();
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.entity = LegalPerson.decode(reader, reader.uint32());
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.contacts = Contacts.decode(reader, reader.uint32());
          continue;
        case 5:
          if (tag != 42) {
            break;
          }

          message.identityCertificate = Certificate.decode(reader, reader.uint32());
          continue;
        case 6:
          if (tag != 50) {
            break;
          }

          message.signingCertificates.push(Certificate.decode(reader, reader.uint32()));
          continue;
        case 7:
          if (tag != 58) {
            break;
          }

          message.commonName = reader.string();
          continue;
        case 8:
          if (tag != 66) {
            break;
          }

          message.trisaEndpoint = reader.string();
          continue;
        case 9:
          if (tag != 74) {
            break;
          }

          message.website = reader.string();
          continue;
        case 10:
          if (tag != 80) {
            break;
          }

          message.businessCategory = reader.int32() as any;
          continue;
        case 11:
          if (tag != 90) {
            break;
          }

          message.vaspCategories.push(reader.string());
          continue;
        case 12:
          if (tag != 98) {
            break;
          }

          message.establishedOn = reader.string();
          continue;
        case 13:
          if (tag != 106) {
            break;
          }

          message.trixo = TRIXOQuestionnaire.decode(reader, reader.uint32());
          continue;
        case 14:
          if (tag != 112) {
            break;
          }

          message.verificationStatus = reader.int32() as any;
          continue;
        case 15:
          if (tag != 120) {
            break;
          }

          message.serviceStatus = reader.int32() as any;
          continue;
        case 16:
          if (tag != 130) {
            break;
          }

          message.verifiedOn = reader.string();
          continue;
        case 17:
          if (tag != 138) {
            break;
          }

          message.firstListed = reader.string();
          continue;
        case 18:
          if (tag != 146) {
            break;
          }

          message.lastUpdated = reader.string();
          continue;
        case 19:
          if (tag != 154) {
            break;
          }

          message.signature = reader.bytes() as Buffer;
          continue;
        case 20:
          if (tag != 162) {
            break;
          }

          message.version = Version.decode(reader, reader.uint32());
          continue;
        case 21:
          if (tag != 170) {
            break;
          }

          message.extra = Any.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): VASP {
    return {
      id: isSet(object.id) ? String(object.id) : "",
      registeredDirectory: isSet(object.registeredDirectory) ? String(object.registeredDirectory) : "",
      entity: isSet(object.entity) ? LegalPerson.fromJSON(object.entity) : undefined,
      contacts: isSet(object.contacts) ? Contacts.fromJSON(object.contacts) : undefined,
      identityCertificate: isSet(object.identityCertificate)
        ? Certificate.fromJSON(object.identityCertificate)
        : undefined,
      signingCertificates: Array.isArray(object?.signingCertificates)
        ? object.signingCertificates.map((e: any) => Certificate.fromJSON(e))
        : [],
      commonName: isSet(object.commonName) ? String(object.commonName) : "",
      trisaEndpoint: isSet(object.trisaEndpoint) ? String(object.trisaEndpoint) : "",
      website: isSet(object.website) ? String(object.website) : "",
      businessCategory: isSet(object.businessCategory) ? businessCategoryFromJSON(object.businessCategory) : 0,
      vaspCategories: Array.isArray(object?.vaspCategories) ? object.vaspCategories.map((e: any) => String(e)) : [],
      establishedOn: isSet(object.establishedOn) ? String(object.establishedOn) : "",
      trixo: isSet(object.trixo) ? TRIXOQuestionnaire.fromJSON(object.trixo) : undefined,
      verificationStatus: isSet(object.verificationStatus) ? verificationStateFromJSON(object.verificationStatus) : 0,
      serviceStatus: isSet(object.serviceStatus) ? serviceStateFromJSON(object.serviceStatus) : 0,
      verifiedOn: isSet(object.verifiedOn) ? String(object.verifiedOn) : "",
      firstListed: isSet(object.firstListed) ? String(object.firstListed) : "",
      lastUpdated: isSet(object.lastUpdated) ? String(object.lastUpdated) : "",
      signature: isSet(object.signature) ? Buffer.from(bytesFromBase64(object.signature)) : Buffer.alloc(0),
      version: isSet(object.version) ? Version.fromJSON(object.version) : undefined,
      extra: isSet(object.extra) ? Any.fromJSON(object.extra) : undefined,
    };
  },

  toJSON(message: VASP): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.registeredDirectory !== undefined && (obj.registeredDirectory = message.registeredDirectory);
    message.entity !== undefined && (obj.entity = message.entity ? LegalPerson.toJSON(message.entity) : undefined);
    message.contacts !== undefined && (obj.contacts = message.contacts ? Contacts.toJSON(message.contacts) : undefined);
    message.identityCertificate !== undefined && (obj.identityCertificate = message.identityCertificate
      ? Certificate.toJSON(message.identityCertificate)
      : undefined);
    if (message.signingCertificates) {
      obj.signingCertificates = message.signingCertificates.map((e) => e ? Certificate.toJSON(e) : undefined);
    } else {
      obj.signingCertificates = [];
    }
    message.commonName !== undefined && (obj.commonName = message.commonName);
    message.trisaEndpoint !== undefined && (obj.trisaEndpoint = message.trisaEndpoint);
    message.website !== undefined && (obj.website = message.website);
    message.businessCategory !== undefined && (obj.businessCategory = businessCategoryToJSON(message.businessCategory));
    if (message.vaspCategories) {
      obj.vaspCategories = message.vaspCategories.map((e) => e);
    } else {
      obj.vaspCategories = [];
    }
    message.establishedOn !== undefined && (obj.establishedOn = message.establishedOn);
    message.trixo !== undefined && (obj.trixo = message.trixo ? TRIXOQuestionnaire.toJSON(message.trixo) : undefined);
    message.verificationStatus !== undefined &&
      (obj.verificationStatus = verificationStateToJSON(message.verificationStatus));
    message.serviceStatus !== undefined && (obj.serviceStatus = serviceStateToJSON(message.serviceStatus));
    message.verifiedOn !== undefined && (obj.verifiedOn = message.verifiedOn);
    message.firstListed !== undefined && (obj.firstListed = message.firstListed);
    message.lastUpdated !== undefined && (obj.lastUpdated = message.lastUpdated);
    message.signature !== undefined &&
      (obj.signature = base64FromBytes(message.signature !== undefined ? message.signature : Buffer.alloc(0)));
    message.version !== undefined && (obj.version = message.version ? Version.toJSON(message.version) : undefined);
    message.extra !== undefined && (obj.extra = message.extra ? Any.toJSON(message.extra) : undefined);
    return obj;
  },

  create<I extends Exact<DeepPartial<VASP>, I>>(base?: I): VASP {
    return VASP.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<VASP>, I>>(object: I): VASP {
    const message = createBaseVASP();
    message.id = object.id ?? "";
    message.registeredDirectory = object.registeredDirectory ?? "";
    message.entity = (object.entity !== undefined && object.entity !== null)
      ? LegalPerson.fromPartial(object.entity)
      : undefined;
    message.contacts = (object.contacts !== undefined && object.contacts !== null)
      ? Contacts.fromPartial(object.contacts)
      : undefined;
    message.identityCertificate = (object.identityCertificate !== undefined && object.identityCertificate !== null)
      ? Certificate.fromPartial(object.identityCertificate)
      : undefined;
    message.signingCertificates = object.signingCertificates?.map((e) => Certificate.fromPartial(e)) || [];
    message.commonName = object.commonName ?? "";
    message.trisaEndpoint = object.trisaEndpoint ?? "";
    message.website = object.website ?? "";
    message.businessCategory = object.businessCategory ?? 0;
    message.vaspCategories = object.vaspCategories?.map((e) => e) || [];
    message.establishedOn = object.establishedOn ?? "";
    message.trixo = (object.trixo !== undefined && object.trixo !== null)
      ? TRIXOQuestionnaire.fromPartial(object.trixo)
      : undefined;
    message.verificationStatus = object.verificationStatus ?? 0;
    message.serviceStatus = object.serviceStatus ?? 0;
    message.verifiedOn = object.verifiedOn ?? "";
    message.firstListed = object.firstListed ?? "";
    message.lastUpdated = object.lastUpdated ?? "";
    message.signature = object.signature ?? Buffer.alloc(0);
    message.version = (object.version !== undefined && object.version !== null)
      ? Version.fromPartial(object.version)
      : undefined;
    message.extra = (object.extra !== undefined && object.extra !== null) ? Any.fromPartial(object.extra) : undefined;
    return message;
  },
};

function createBaseContacts(): Contacts {
  return { technical: undefined, administrative: undefined, legal: undefined, billing: undefined };
}

export const Contacts = {
  encode(message: Contacts, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.technical !== undefined) {
      Contact.encode(message.technical, writer.uint32(10).fork()).ldelim();
    }
    if (message.administrative !== undefined) {
      Contact.encode(message.administrative, writer.uint32(18).fork()).ldelim();
    }
    if (message.legal !== undefined) {
      Contact.encode(message.legal, writer.uint32(26).fork()).ldelim();
    }
    if (message.billing !== undefined) {
      Contact.encode(message.billing, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Contacts {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseContacts();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.technical = Contact.decode(reader, reader.uint32());
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.administrative = Contact.decode(reader, reader.uint32());
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.legal = Contact.decode(reader, reader.uint32());
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.billing = Contact.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Contacts {
    return {
      technical: isSet(object.technical) ? Contact.fromJSON(object.technical) : undefined,
      administrative: isSet(object.administrative) ? Contact.fromJSON(object.administrative) : undefined,
      legal: isSet(object.legal) ? Contact.fromJSON(object.legal) : undefined,
      billing: isSet(object.billing) ? Contact.fromJSON(object.billing) : undefined,
    };
  },

  toJSON(message: Contacts): unknown {
    const obj: any = {};
    message.technical !== undefined &&
      (obj.technical = message.technical ? Contact.toJSON(message.technical) : undefined);
    message.administrative !== undefined &&
      (obj.administrative = message.administrative ? Contact.toJSON(message.administrative) : undefined);
    message.legal !== undefined && (obj.legal = message.legal ? Contact.toJSON(message.legal) : undefined);
    message.billing !== undefined && (obj.billing = message.billing ? Contact.toJSON(message.billing) : undefined);
    return obj;
  },

  create<I extends Exact<DeepPartial<Contacts>, I>>(base?: I): Contacts {
    return Contacts.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Contacts>, I>>(object: I): Contacts {
    const message = createBaseContacts();
    message.technical = (object.technical !== undefined && object.technical !== null)
      ? Contact.fromPartial(object.technical)
      : undefined;
    message.administrative = (object.administrative !== undefined && object.administrative !== null)
      ? Contact.fromPartial(object.administrative)
      : undefined;
    message.legal = (object.legal !== undefined && object.legal !== null)
      ? Contact.fromPartial(object.legal)
      : undefined;
    message.billing = (object.billing !== undefined && object.billing !== null)
      ? Contact.fromPartial(object.billing)
      : undefined;
    return message;
  },
};

function createBaseContact(): Contact {
  return { name: "", email: "", phone: "", person: undefined, extra: undefined };
}

export const Contact = {
  encode(message: Contact, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.name !== "") {
      writer.uint32(10).string(message.name);
    }
    if (message.email !== "") {
      writer.uint32(18).string(message.email);
    }
    if (message.phone !== "") {
      writer.uint32(26).string(message.phone);
    }
    if (message.person !== undefined) {
      NaturalPerson.encode(message.person, writer.uint32(34).fork()).ldelim();
    }
    if (message.extra !== undefined) {
      Any.encode(message.extra, writer.uint32(42).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Contact {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseContact();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.name = reader.string();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.email = reader.string();
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.phone = reader.string();
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.person = NaturalPerson.decode(reader, reader.uint32());
          continue;
        case 5:
          if (tag != 42) {
            break;
          }

          message.extra = Any.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Contact {
    return {
      name: isSet(object.name) ? String(object.name) : "",
      email: isSet(object.email) ? String(object.email) : "",
      phone: isSet(object.phone) ? String(object.phone) : "",
      person: isSet(object.person) ? NaturalPerson.fromJSON(object.person) : undefined,
      extra: isSet(object.extra) ? Any.fromJSON(object.extra) : undefined,
    };
  },

  toJSON(message: Contact): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name);
    message.email !== undefined && (obj.email = message.email);
    message.phone !== undefined && (obj.phone = message.phone);
    message.person !== undefined && (obj.person = message.person ? NaturalPerson.toJSON(message.person) : undefined);
    message.extra !== undefined && (obj.extra = message.extra ? Any.toJSON(message.extra) : undefined);
    return obj;
  },

  create<I extends Exact<DeepPartial<Contact>, I>>(base?: I): Contact {
    return Contact.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Contact>, I>>(object: I): Contact {
    const message = createBaseContact();
    message.name = object.name ?? "";
    message.email = object.email ?? "";
    message.phone = object.phone ?? "";
    message.person = (object.person !== undefined && object.person !== null)
      ? NaturalPerson.fromPartial(object.person)
      : undefined;
    message.extra = (object.extra !== undefined && object.extra !== null) ? Any.fromPartial(object.extra) : undefined;
    return message;
  },
};

function createBaseTRIXOQuestionnaire(): TRIXOQuestionnaire {
  return {
    primaryNationalJurisdiction: "",
    primaryRegulator: "",
    financialTransfersPermitted: "",
    otherJurisdictions: [],
    hasRequiredRegulatoryProgram: "",
    conductsCustomerKyc: false,
    kycThreshold: 0,
    kycThresholdCurrency: "",
    mustComplyTravelRule: false,
    applicableRegulations: [],
    complianceThreshold: 0,
    complianceThresholdCurrency: "",
    mustSafeguardPii: false,
    safeguardsPii: false,
  };
}

export const TRIXOQuestionnaire = {
  encode(message: TRIXOQuestionnaire, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.primaryNationalJurisdiction !== "") {
      writer.uint32(10).string(message.primaryNationalJurisdiction);
    }
    if (message.primaryRegulator !== "") {
      writer.uint32(18).string(message.primaryRegulator);
    }
    if (message.financialTransfersPermitted !== "") {
      writer.uint32(26).string(message.financialTransfersPermitted);
    }
    for (const v of message.otherJurisdictions) {
      Jurisdiction.encode(v!, writer.uint32(34).fork()).ldelim();
    }
    if (message.hasRequiredRegulatoryProgram !== "") {
      writer.uint32(42).string(message.hasRequiredRegulatoryProgram);
    }
    if (message.conductsCustomerKyc === true) {
      writer.uint32(48).bool(message.conductsCustomerKyc);
    }
    if (message.kycThreshold !== 0) {
      writer.uint32(61).float(message.kycThreshold);
    }
    if (message.kycThresholdCurrency !== "") {
      writer.uint32(66).string(message.kycThresholdCurrency);
    }
    if (message.mustComplyTravelRule === true) {
      writer.uint32(72).bool(message.mustComplyTravelRule);
    }
    for (const v of message.applicableRegulations) {
      writer.uint32(82).string(v!);
    }
    if (message.complianceThreshold !== 0) {
      writer.uint32(93).float(message.complianceThreshold);
    }
    if (message.complianceThresholdCurrency !== "") {
      writer.uint32(98).string(message.complianceThresholdCurrency);
    }
    if (message.mustSafeguardPii === true) {
      writer.uint32(104).bool(message.mustSafeguardPii);
    }
    if (message.safeguardsPii === true) {
      writer.uint32(112).bool(message.safeguardsPii);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TRIXOQuestionnaire {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTRIXOQuestionnaire();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.primaryNationalJurisdiction = reader.string();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.primaryRegulator = reader.string();
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.financialTransfersPermitted = reader.string();
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.otherJurisdictions.push(Jurisdiction.decode(reader, reader.uint32()));
          continue;
        case 5:
          if (tag != 42) {
            break;
          }

          message.hasRequiredRegulatoryProgram = reader.string();
          continue;
        case 6:
          if (tag != 48) {
            break;
          }

          message.conductsCustomerKyc = reader.bool();
          continue;
        case 7:
          if (tag != 61) {
            break;
          }

          message.kycThreshold = reader.float();
          continue;
        case 8:
          if (tag != 66) {
            break;
          }

          message.kycThresholdCurrency = reader.string();
          continue;
        case 9:
          if (tag != 72) {
            break;
          }

          message.mustComplyTravelRule = reader.bool();
          continue;
        case 10:
          if (tag != 82) {
            break;
          }

          message.applicableRegulations.push(reader.string());
          continue;
        case 11:
          if (tag != 93) {
            break;
          }

          message.complianceThreshold = reader.float();
          continue;
        case 12:
          if (tag != 98) {
            break;
          }

          message.complianceThresholdCurrency = reader.string();
          continue;
        case 13:
          if (tag != 104) {
            break;
          }

          message.mustSafeguardPii = reader.bool();
          continue;
        case 14:
          if (tag != 112) {
            break;
          }

          message.safeguardsPii = reader.bool();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): TRIXOQuestionnaire {
    return {
      primaryNationalJurisdiction: isSet(object.primaryNationalJurisdiction)
        ? String(object.primaryNationalJurisdiction)
        : "",
      primaryRegulator: isSet(object.primaryRegulator) ? String(object.primaryRegulator) : "",
      financialTransfersPermitted: isSet(object.financialTransfersPermitted)
        ? String(object.financialTransfersPermitted)
        : "",
      otherJurisdictions: Array.isArray(object?.otherJurisdictions)
        ? object.otherJurisdictions.map((e: any) => Jurisdiction.fromJSON(e))
        : [],
      hasRequiredRegulatoryProgram: isSet(object.hasRequiredRegulatoryProgram)
        ? String(object.hasRequiredRegulatoryProgram)
        : "",
      conductsCustomerKyc: isSet(object.conductsCustomerKyc) ? Boolean(object.conductsCustomerKyc) : false,
      kycThreshold: isSet(object.kycThreshold) ? Number(object.kycThreshold) : 0,
      kycThresholdCurrency: isSet(object.kycThresholdCurrency) ? String(object.kycThresholdCurrency) : "",
      mustComplyTravelRule: isSet(object.mustComplyTravelRule) ? Boolean(object.mustComplyTravelRule) : false,
      applicableRegulations: Array.isArray(object?.applicableRegulations)
        ? object.applicableRegulations.map((e: any) => String(e))
        : [],
      complianceThreshold: isSet(object.complianceThreshold) ? Number(object.complianceThreshold) : 0,
      complianceThresholdCurrency: isSet(object.complianceThresholdCurrency)
        ? String(object.complianceThresholdCurrency)
        : "",
      mustSafeguardPii: isSet(object.mustSafeguardPii) ? Boolean(object.mustSafeguardPii) : false,
      safeguardsPii: isSet(object.safeguardsPii) ? Boolean(object.safeguardsPii) : false,
    };
  },

  toJSON(message: TRIXOQuestionnaire): unknown {
    const obj: any = {};
    message.primaryNationalJurisdiction !== undefined &&
      (obj.primaryNationalJurisdiction = message.primaryNationalJurisdiction);
    message.primaryRegulator !== undefined && (obj.primaryRegulator = message.primaryRegulator);
    message.financialTransfersPermitted !== undefined &&
      (obj.financialTransfersPermitted = message.financialTransfersPermitted);
    if (message.otherJurisdictions) {
      obj.otherJurisdictions = message.otherJurisdictions.map((e) => e ? Jurisdiction.toJSON(e) : undefined);
    } else {
      obj.otherJurisdictions = [];
    }
    message.hasRequiredRegulatoryProgram !== undefined &&
      (obj.hasRequiredRegulatoryProgram = message.hasRequiredRegulatoryProgram);
    message.conductsCustomerKyc !== undefined && (obj.conductsCustomerKyc = message.conductsCustomerKyc);
    message.kycThreshold !== undefined && (obj.kycThreshold = message.kycThreshold);
    message.kycThresholdCurrency !== undefined && (obj.kycThresholdCurrency = message.kycThresholdCurrency);
    message.mustComplyTravelRule !== undefined && (obj.mustComplyTravelRule = message.mustComplyTravelRule);
    if (message.applicableRegulations) {
      obj.applicableRegulations = message.applicableRegulations.map((e) => e);
    } else {
      obj.applicableRegulations = [];
    }
    message.complianceThreshold !== undefined && (obj.complianceThreshold = message.complianceThreshold);
    message.complianceThresholdCurrency !== undefined &&
      (obj.complianceThresholdCurrency = message.complianceThresholdCurrency);
    message.mustSafeguardPii !== undefined && (obj.mustSafeguardPii = message.mustSafeguardPii);
    message.safeguardsPii !== undefined && (obj.safeguardsPii = message.safeguardsPii);
    return obj;
  },

  create<I extends Exact<DeepPartial<TRIXOQuestionnaire>, I>>(base?: I): TRIXOQuestionnaire {
    return TRIXOQuestionnaire.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<TRIXOQuestionnaire>, I>>(object: I): TRIXOQuestionnaire {
    const message = createBaseTRIXOQuestionnaire();
    message.primaryNationalJurisdiction = object.primaryNationalJurisdiction ?? "";
    message.primaryRegulator = object.primaryRegulator ?? "";
    message.financialTransfersPermitted = object.financialTransfersPermitted ?? "";
    message.otherJurisdictions = object.otherJurisdictions?.map((e) => Jurisdiction.fromPartial(e)) || [];
    message.hasRequiredRegulatoryProgram = object.hasRequiredRegulatoryProgram ?? "";
    message.conductsCustomerKyc = object.conductsCustomerKyc ?? false;
    message.kycThreshold = object.kycThreshold ?? 0;
    message.kycThresholdCurrency = object.kycThresholdCurrency ?? "";
    message.mustComplyTravelRule = object.mustComplyTravelRule ?? false;
    message.applicableRegulations = object.applicableRegulations?.map((e) => e) || [];
    message.complianceThreshold = object.complianceThreshold ?? 0;
    message.complianceThresholdCurrency = object.complianceThresholdCurrency ?? "";
    message.mustSafeguardPii = object.mustSafeguardPii ?? false;
    message.safeguardsPii = object.safeguardsPii ?? false;
    return message;
  },
};

function createBaseJurisdiction(): Jurisdiction {
  return { country: "", regulatorName: "", licenseNumber: "" };
}

export const Jurisdiction = {
  encode(message: Jurisdiction, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.country !== "") {
      writer.uint32(10).string(message.country);
    }
    if (message.regulatorName !== "") {
      writer.uint32(18).string(message.regulatorName);
    }
    if (message.licenseNumber !== "") {
      writer.uint32(26).string(message.licenseNumber);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Jurisdiction {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseJurisdiction();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.country = reader.string();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.regulatorName = reader.string();
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.licenseNumber = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Jurisdiction {
    return {
      country: isSet(object.country) ? String(object.country) : "",
      regulatorName: isSet(object.regulatorName) ? String(object.regulatorName) : "",
      licenseNumber: isSet(object.licenseNumber) ? String(object.licenseNumber) : "",
    };
  },

  toJSON(message: Jurisdiction): unknown {
    const obj: any = {};
    message.country !== undefined && (obj.country = message.country);
    message.regulatorName !== undefined && (obj.regulatorName = message.regulatorName);
    message.licenseNumber !== undefined && (obj.licenseNumber = message.licenseNumber);
    return obj;
  },

  create<I extends Exact<DeepPartial<Jurisdiction>, I>>(base?: I): Jurisdiction {
    return Jurisdiction.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Jurisdiction>, I>>(object: I): Jurisdiction {
    const message = createBaseJurisdiction();
    message.country = object.country ?? "";
    message.regulatorName = object.regulatorName ?? "";
    message.licenseNumber = object.licenseNumber ?? "";
    return message;
  },
};

function createBaseVersion(): Version {
  return { pid: 0, version: 0 };
}

export const Version = {
  encode(message: Version, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pid !== 0) {
      writer.uint32(8).uint64(message.pid);
    }
    if (message.version !== 0) {
      writer.uint32(16).uint64(message.version);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Version {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseVersion();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.pid = longToNumber(reader.uint64() as Long);
          continue;
        case 2:
          if (tag != 16) {
            break;
          }

          message.version = longToNumber(reader.uint64() as Long);
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Version {
    return {
      pid: isSet(object.pid) ? Number(object.pid) : 0,
      version: isSet(object.version) ? Number(object.version) : 0,
    };
  },

  toJSON(message: Version): unknown {
    const obj: any = {};
    message.pid !== undefined && (obj.pid = Math.round(message.pid));
    message.version !== undefined && (obj.version = Math.round(message.version));
    return obj;
  },

  create<I extends Exact<DeepPartial<Version>, I>>(base?: I): Version {
    return Version.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Version>, I>>(object: I): Version {
    const message = createBaseVersion();
    message.pid = object.pid ?? 0;
    message.version = object.version ?? 0;
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

function bytesFromBase64(b64: string): Uint8Array {
  if (tsProtoGlobalThis.Buffer) {
    return Uint8Array.from(tsProtoGlobalThis.Buffer.from(b64, "base64"));
  } else {
    const bin = tsProtoGlobalThis.atob(b64);
    const arr = new Uint8Array(bin.length);
    for (let i = 0; i < bin.length; ++i) {
      arr[i] = bin.charCodeAt(i);
    }
    return arr;
  }
}

function base64FromBytes(arr: Uint8Array): string {
  if (tsProtoGlobalThis.Buffer) {
    return tsProtoGlobalThis.Buffer.from(arr).toString("base64");
  } else {
    const bin: string[] = [];
    arr.forEach((byte) => {
      bin.push(String.fromCharCode(byte));
    });
    return tsProtoGlobalThis.btoa(bin.join(""));
  }
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
