/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "trisa.gds.models.v1beta1";

export interface Certificate {
  /**
   * x.509 metadata information for ease of reference
   * note that the serial number as a hex string can be used with the Sectigo API
   */
  version: number;
  serialNumber: Buffer;
  signature: Buffer;
  signatureAlgorithm: string;
  publicKeyAlgorithm: string;
  /** Issuer and subject information from Sectigo */
  subject: Name | undefined;
  issuer:
    | Name
    | undefined;
  /** Validity information */
  notBefore: string;
  notAfter: string;
  revoked: boolean;
  /** The ASN1 encoded full certificate without the trust chain */
  data: Buffer;
  /**
   * The complete trust chain including the leaf certificate as a gzip compressed
   * PEM encoded file. This field can be deserialized into a trust.Provider.
   */
  chain: Buffer;
}

/** An X.509 distinguished name with the common elements of a DN. */
export interface Name {
  commonName: string;
  serialNumber: string;
  organization: string[];
  organizationalUnit: string[];
  streetAddress: string[];
  locality: string[];
  province: string[];
  postalCode: string[];
  country: string[];
}

function createBaseCertificate(): Certificate {
  return {
    version: 0,
    serialNumber: Buffer.alloc(0),
    signature: Buffer.alloc(0),
    signatureAlgorithm: "",
    publicKeyAlgorithm: "",
    subject: undefined,
    issuer: undefined,
    notBefore: "",
    notAfter: "",
    revoked: false,
    data: Buffer.alloc(0),
    chain: Buffer.alloc(0),
  };
}

export const Certificate = {
  encode(message: Certificate, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.version !== 0) {
      writer.uint32(8).int64(message.version);
    }
    if (message.serialNumber.length !== 0) {
      writer.uint32(18).bytes(message.serialNumber);
    }
    if (message.signature.length !== 0) {
      writer.uint32(26).bytes(message.signature);
    }
    if (message.signatureAlgorithm !== "") {
      writer.uint32(34).string(message.signatureAlgorithm);
    }
    if (message.publicKeyAlgorithm !== "") {
      writer.uint32(42).string(message.publicKeyAlgorithm);
    }
    if (message.subject !== undefined) {
      Name.encode(message.subject, writer.uint32(50).fork()).ldelim();
    }
    if (message.issuer !== undefined) {
      Name.encode(message.issuer, writer.uint32(58).fork()).ldelim();
    }
    if (message.notBefore !== "") {
      writer.uint32(66).string(message.notBefore);
    }
    if (message.notAfter !== "") {
      writer.uint32(74).string(message.notAfter);
    }
    if (message.revoked === true) {
      writer.uint32(80).bool(message.revoked);
    }
    if (message.data.length !== 0) {
      writer.uint32(90).bytes(message.data);
    }
    if (message.chain.length !== 0) {
      writer.uint32(98).bytes(message.chain);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Certificate {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCertificate();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.version = longToNumber(reader.int64() as Long);
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.serialNumber = reader.bytes() as Buffer;
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.signature = reader.bytes() as Buffer;
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.signatureAlgorithm = reader.string();
          continue;
        case 5:
          if (tag != 42) {
            break;
          }

          message.publicKeyAlgorithm = reader.string();
          continue;
        case 6:
          if (tag != 50) {
            break;
          }

          message.subject = Name.decode(reader, reader.uint32());
          continue;
        case 7:
          if (tag != 58) {
            break;
          }

          message.issuer = Name.decode(reader, reader.uint32());
          continue;
        case 8:
          if (tag != 66) {
            break;
          }

          message.notBefore = reader.string();
          continue;
        case 9:
          if (tag != 74) {
            break;
          }

          message.notAfter = reader.string();
          continue;
        case 10:
          if (tag != 80) {
            break;
          }

          message.revoked = reader.bool();
          continue;
        case 11:
          if (tag != 90) {
            break;
          }

          message.data = reader.bytes() as Buffer;
          continue;
        case 12:
          if (tag != 98) {
            break;
          }

          message.chain = reader.bytes() as Buffer;
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Certificate {
    return {
      version: isSet(object.version) ? Number(object.version) : 0,
      serialNumber: isSet(object.serialNumber) ? Buffer.from(bytesFromBase64(object.serialNumber)) : Buffer.alloc(0),
      signature: isSet(object.signature) ? Buffer.from(bytesFromBase64(object.signature)) : Buffer.alloc(0),
      signatureAlgorithm: isSet(object.signatureAlgorithm) ? String(object.signatureAlgorithm) : "",
      publicKeyAlgorithm: isSet(object.publicKeyAlgorithm) ? String(object.publicKeyAlgorithm) : "",
      subject: isSet(object.subject) ? Name.fromJSON(object.subject) : undefined,
      issuer: isSet(object.issuer) ? Name.fromJSON(object.issuer) : undefined,
      notBefore: isSet(object.notBefore) ? String(object.notBefore) : "",
      notAfter: isSet(object.notAfter) ? String(object.notAfter) : "",
      revoked: isSet(object.revoked) ? Boolean(object.revoked) : false,
      data: isSet(object.data) ? Buffer.from(bytesFromBase64(object.data)) : Buffer.alloc(0),
      chain: isSet(object.chain) ? Buffer.from(bytesFromBase64(object.chain)) : Buffer.alloc(0),
    };
  },

  toJSON(message: Certificate): unknown {
    const obj: any = {};
    message.version !== undefined && (obj.version = Math.round(message.version));
    message.serialNumber !== undefined &&
      (obj.serialNumber = base64FromBytes(message.serialNumber !== undefined ? message.serialNumber : Buffer.alloc(0)));
    message.signature !== undefined &&
      (obj.signature = base64FromBytes(message.signature !== undefined ? message.signature : Buffer.alloc(0)));
    message.signatureAlgorithm !== undefined && (obj.signatureAlgorithm = message.signatureAlgorithm);
    message.publicKeyAlgorithm !== undefined && (obj.publicKeyAlgorithm = message.publicKeyAlgorithm);
    message.subject !== undefined && (obj.subject = message.subject ? Name.toJSON(message.subject) : undefined);
    message.issuer !== undefined && (obj.issuer = message.issuer ? Name.toJSON(message.issuer) : undefined);
    message.notBefore !== undefined && (obj.notBefore = message.notBefore);
    message.notAfter !== undefined && (obj.notAfter = message.notAfter);
    message.revoked !== undefined && (obj.revoked = message.revoked);
    message.data !== undefined &&
      (obj.data = base64FromBytes(message.data !== undefined ? message.data : Buffer.alloc(0)));
    message.chain !== undefined &&
      (obj.chain = base64FromBytes(message.chain !== undefined ? message.chain : Buffer.alloc(0)));
    return obj;
  },

  create<I extends Exact<DeepPartial<Certificate>, I>>(base?: I): Certificate {
    return Certificate.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Certificate>, I>>(object: I): Certificate {
    const message = createBaseCertificate();
    message.version = object.version ?? 0;
    message.serialNumber = object.serialNumber ?? Buffer.alloc(0);
    message.signature = object.signature ?? Buffer.alloc(0);
    message.signatureAlgorithm = object.signatureAlgorithm ?? "";
    message.publicKeyAlgorithm = object.publicKeyAlgorithm ?? "";
    message.subject = (object.subject !== undefined && object.subject !== null)
      ? Name.fromPartial(object.subject)
      : undefined;
    message.issuer = (object.issuer !== undefined && object.issuer !== null)
      ? Name.fromPartial(object.issuer)
      : undefined;
    message.notBefore = object.notBefore ?? "";
    message.notAfter = object.notAfter ?? "";
    message.revoked = object.revoked ?? false;
    message.data = object.data ?? Buffer.alloc(0);
    message.chain = object.chain ?? Buffer.alloc(0);
    return message;
  },
};

function createBaseName(): Name {
  return {
    commonName: "",
    serialNumber: "",
    organization: [],
    organizationalUnit: [],
    streetAddress: [],
    locality: [],
    province: [],
    postalCode: [],
    country: [],
  };
}

export const Name = {
  encode(message: Name, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.commonName !== "") {
      writer.uint32(10).string(message.commonName);
    }
    if (message.serialNumber !== "") {
      writer.uint32(18).string(message.serialNumber);
    }
    for (const v of message.organization) {
      writer.uint32(26).string(v!);
    }
    for (const v of message.organizationalUnit) {
      writer.uint32(34).string(v!);
    }
    for (const v of message.streetAddress) {
      writer.uint32(42).string(v!);
    }
    for (const v of message.locality) {
      writer.uint32(50).string(v!);
    }
    for (const v of message.province) {
      writer.uint32(58).string(v!);
    }
    for (const v of message.postalCode) {
      writer.uint32(66).string(v!);
    }
    for (const v of message.country) {
      writer.uint32(74).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Name {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseName();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.commonName = reader.string();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.serialNumber = reader.string();
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.organization.push(reader.string());
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.organizationalUnit.push(reader.string());
          continue;
        case 5:
          if (tag != 42) {
            break;
          }

          message.streetAddress.push(reader.string());
          continue;
        case 6:
          if (tag != 50) {
            break;
          }

          message.locality.push(reader.string());
          continue;
        case 7:
          if (tag != 58) {
            break;
          }

          message.province.push(reader.string());
          continue;
        case 8:
          if (tag != 66) {
            break;
          }

          message.postalCode.push(reader.string());
          continue;
        case 9:
          if (tag != 74) {
            break;
          }

          message.country.push(reader.string());
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Name {
    return {
      commonName: isSet(object.commonName) ? String(object.commonName) : "",
      serialNumber: isSet(object.serialNumber) ? String(object.serialNumber) : "",
      organization: Array.isArray(object?.organization) ? object.organization.map((e: any) => String(e)) : [],
      organizationalUnit: Array.isArray(object?.organizationalUnit)
        ? object.organizationalUnit.map((e: any) => String(e))
        : [],
      streetAddress: Array.isArray(object?.streetAddress) ? object.streetAddress.map((e: any) => String(e)) : [],
      locality: Array.isArray(object?.locality) ? object.locality.map((e: any) => String(e)) : [],
      province: Array.isArray(object?.province) ? object.province.map((e: any) => String(e)) : [],
      postalCode: Array.isArray(object?.postalCode) ? object.postalCode.map((e: any) => String(e)) : [],
      country: Array.isArray(object?.country) ? object.country.map((e: any) => String(e)) : [],
    };
  },

  toJSON(message: Name): unknown {
    const obj: any = {};
    message.commonName !== undefined && (obj.commonName = message.commonName);
    message.serialNumber !== undefined && (obj.serialNumber = message.serialNumber);
    if (message.organization) {
      obj.organization = message.organization.map((e) => e);
    } else {
      obj.organization = [];
    }
    if (message.organizationalUnit) {
      obj.organizationalUnit = message.organizationalUnit.map((e) => e);
    } else {
      obj.organizationalUnit = [];
    }
    if (message.streetAddress) {
      obj.streetAddress = message.streetAddress.map((e) => e);
    } else {
      obj.streetAddress = [];
    }
    if (message.locality) {
      obj.locality = message.locality.map((e) => e);
    } else {
      obj.locality = [];
    }
    if (message.province) {
      obj.province = message.province.map((e) => e);
    } else {
      obj.province = [];
    }
    if (message.postalCode) {
      obj.postalCode = message.postalCode.map((e) => e);
    } else {
      obj.postalCode = [];
    }
    if (message.country) {
      obj.country = message.country.map((e) => e);
    } else {
      obj.country = [];
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<Name>, I>>(base?: I): Name {
    return Name.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Name>, I>>(object: I): Name {
    const message = createBaseName();
    message.commonName = object.commonName ?? "";
    message.serialNumber = object.serialNumber ?? "";
    message.organization = object.organization?.map((e) => e) || [];
    message.organizationalUnit = object.organizationalUnit?.map((e) => e) || [];
    message.streetAddress = object.streetAddress?.map((e) => e) || [];
    message.locality = object.locality?.map((e) => e) || [];
    message.province = object.province?.map((e) => e) || [];
    message.postalCode = object.postalCode?.map((e) => e) || [];
    message.country = object.country?.map((e) => e) || [];
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

function bytesFromBase64(b64: string): Uint8Array {
  if (tsProtoGlobalThis.Buffer) {
    return Uint8Array.from(tsProtoGlobalThis.Buffer.from(b64, "base64"));
  } else {
    const bin = tsProtoGlobalThis.atob(b64);
    const arr = new Uint8Array(bin.length);
    for (let i = 0; i < bin.length; ++i) {
      arr[i] = bin.charCodeAt(i);
    }
    return arr;
  }
}

function base64FromBytes(arr: Uint8Array): string {
  if (tsProtoGlobalThis.Buffer) {
    return tsProtoGlobalThis.Buffer.from(arr).toString("base64");
  } else {
    const bin: string[] = [];
    arr.forEach((byte) => {
      bin.push(String.fromCharCode(byte));
    });
    return tsProtoGlobalThis.btoa(bin.join(""));
  }
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
