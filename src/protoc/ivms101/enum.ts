/* eslint-disable */

export const protobufPackage = "ivms101";

/** Definition: A single value corresponding to the nature of name being adopted. */
export enum NaturalPersonNameTypeCode {
  /**
   * NATURAL_PERSON_NAME_TYPE_CODE_MISC - Unspecified
   * A name by which a natural person may be known but which cannot otherwise be
   * categorized or the category of which the sender is unable to determine.
   */
  NATURAL_PERSON_NAME_TYPE_CODE_MISC = 0,
  /**
   * NATURAL_PERSON_NAME_TYPE_CODE_ALIA - Alias name
   * A name other than the legal name by which a natural person is also known.
   */
  NATURAL_PERSON_NAME_TYPE_CODE_ALIA = 1,
  /**
   * NATURAL_PERSON_NAME_TYPE_CODE_BIRT - Name at birth
   * The name given to a natural person at birth.
   */
  NATURAL_PERSON_NAME_TYPE_CODE_BIRT = 2,
  /**
   * NATURAL_PERSON_NAME_TYPE_CODE_MAID - Maiden name
   * The original name of a natural person who has changed their name after marriage.
   */
  NATURAL_PERSON_NAME_TYPE_CODE_MAID = 3,
  /**
   * NATURAL_PERSON_NAME_TYPE_CODE_LEGL - Legal name
   * Identifies a natural person for legal, official or administrative purposes.
   */
  NATURAL_PERSON_NAME_TYPE_CODE_LEGL = 4,
  UNRECOGNIZED = -1,
}

export function naturalPersonNameTypeCodeFromJSON(object: any): NaturalPersonNameTypeCode {
  switch (object) {
    case 0:
    case "NATURAL_PERSON_NAME_TYPE_CODE_MISC":
      return NaturalPersonNameTypeCode.NATURAL_PERSON_NAME_TYPE_CODE_MISC;
    case 1:
    case "NATURAL_PERSON_NAME_TYPE_CODE_ALIA":
      return NaturalPersonNameTypeCode.NATURAL_PERSON_NAME_TYPE_CODE_ALIA;
    case 2:
    case "NATURAL_PERSON_NAME_TYPE_CODE_BIRT":
      return NaturalPersonNameTypeCode.NATURAL_PERSON_NAME_TYPE_CODE_BIRT;
    case 3:
    case "NATURAL_PERSON_NAME_TYPE_CODE_MAID":
      return NaturalPersonNameTypeCode.NATURAL_PERSON_NAME_TYPE_CODE_MAID;
    case 4:
    case "NATURAL_PERSON_NAME_TYPE_CODE_LEGL":
      return NaturalPersonNameTypeCode.NATURAL_PERSON_NAME_TYPE_CODE_LEGL;
    case -1:
    case "UNRECOGNIZED":
    default:
      return NaturalPersonNameTypeCode.UNRECOGNIZED;
  }
}

export function naturalPersonNameTypeCodeToJSON(object: NaturalPersonNameTypeCode): string {
  switch (object) {
    case NaturalPersonNameTypeCode.NATURAL_PERSON_NAME_TYPE_CODE_MISC:
      return "NATURAL_PERSON_NAME_TYPE_CODE_MISC";
    case NaturalPersonNameTypeCode.NATURAL_PERSON_NAME_TYPE_CODE_ALIA:
      return "NATURAL_PERSON_NAME_TYPE_CODE_ALIA";
    case NaturalPersonNameTypeCode.NATURAL_PERSON_NAME_TYPE_CODE_BIRT:
      return "NATURAL_PERSON_NAME_TYPE_CODE_BIRT";
    case NaturalPersonNameTypeCode.NATURAL_PERSON_NAME_TYPE_CODE_MAID:
      return "NATURAL_PERSON_NAME_TYPE_CODE_MAID";
    case NaturalPersonNameTypeCode.NATURAL_PERSON_NAME_TYPE_CODE_LEGL:
      return "NATURAL_PERSON_NAME_TYPE_CODE_LEGL";
    case NaturalPersonNameTypeCode.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

/**
 * Definition: A single value corresponding to the nature of name being specified
 * for the legal person.
 */
export enum LegalPersonNameTypeCode {
  /**
   * LEGAL_PERSON_NAME_TYPE_CODE_MISC - Unspecified
   * A name by which a legal person may be known but which cannot otherwise be
   * categorized or the category of which the sender is unable to determine.
   * Not an official part of the IVMS 101 Standard
   */
  LEGAL_PERSON_NAME_TYPE_CODE_MISC = 0,
  /**
   * LEGAL_PERSON_NAME_TYPE_CODE_LEGL - Legal name
   * Official name under which an organisation is registered.
   */
  LEGAL_PERSON_NAME_TYPE_CODE_LEGL = 1,
  /**
   * LEGAL_PERSON_NAME_TYPE_CODE_SHRT - Short name
   * Specifies the short name of the organisation.
   */
  LEGAL_PERSON_NAME_TYPE_CODE_SHRT = 2,
  /**
   * LEGAL_PERSON_NAME_TYPE_CODE_TRAD - Trading name
   * Name used by a business for commercial purposes, although its registered legal
   * name, used for contracts and other formal situations, may be another.
   */
  LEGAL_PERSON_NAME_TYPE_CODE_TRAD = 3,
  UNRECOGNIZED = -1,
}

export function legalPersonNameTypeCodeFromJSON(object: any): LegalPersonNameTypeCode {
  switch (object) {
    case 0:
    case "LEGAL_PERSON_NAME_TYPE_CODE_MISC":
      return LegalPersonNameTypeCode.LEGAL_PERSON_NAME_TYPE_CODE_MISC;
    case 1:
    case "LEGAL_PERSON_NAME_TYPE_CODE_LEGL":
      return LegalPersonNameTypeCode.LEGAL_PERSON_NAME_TYPE_CODE_LEGL;
    case 2:
    case "LEGAL_PERSON_NAME_TYPE_CODE_SHRT":
      return LegalPersonNameTypeCode.LEGAL_PERSON_NAME_TYPE_CODE_SHRT;
    case 3:
    case "LEGAL_PERSON_NAME_TYPE_CODE_TRAD":
      return LegalPersonNameTypeCode.LEGAL_PERSON_NAME_TYPE_CODE_TRAD;
    case -1:
    case "UNRECOGNIZED":
    default:
      return LegalPersonNameTypeCode.UNRECOGNIZED;
  }
}

export function legalPersonNameTypeCodeToJSON(object: LegalPersonNameTypeCode): string {
  switch (object) {
    case LegalPersonNameTypeCode.LEGAL_PERSON_NAME_TYPE_CODE_MISC:
      return "LEGAL_PERSON_NAME_TYPE_CODE_MISC";
    case LegalPersonNameTypeCode.LEGAL_PERSON_NAME_TYPE_CODE_LEGL:
      return "LEGAL_PERSON_NAME_TYPE_CODE_LEGL";
    case LegalPersonNameTypeCode.LEGAL_PERSON_NAME_TYPE_CODE_SHRT:
      return "LEGAL_PERSON_NAME_TYPE_CODE_SHRT";
    case LegalPersonNameTypeCode.LEGAL_PERSON_NAME_TYPE_CODE_TRAD:
      return "LEGAL_PERSON_NAME_TYPE_CODE_TRAD";
    case LegalPersonNameTypeCode.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

/** Definition: Identifies the nature of the address. */
export enum AddressTypeCode {
  /**
   * ADDRESS_TYPE_CODE_MISC - Unspecified
   * An address the category of which the sender is unable to determine.
   * Use GEOG instead of this code in general use.
   * Not an official part of the IVMS 101 Standard
   */
  ADDRESS_TYPE_CODE_MISC = 0,
  /**
   * ADDRESS_TYPE_CODE_HOME - Residential
   * Address is the home address.
   */
  ADDRESS_TYPE_CODE_HOME = 1,
  /**
   * ADDRESS_TYPE_CODE_BIZZ - Business
   * Address is the business address.
   */
  ADDRESS_TYPE_CODE_BIZZ = 2,
  /**
   * ADDRESS_TYPE_CODE_GEOG - Geographic
   * Address is the unspecified physical (geographical) address suitable for
   * identification of the natural or legal person.
   */
  ADDRESS_TYPE_CODE_GEOG = 3,
  UNRECOGNIZED = -1,
}

export function addressTypeCodeFromJSON(object: any): AddressTypeCode {
  switch (object) {
    case 0:
    case "ADDRESS_TYPE_CODE_MISC":
      return AddressTypeCode.ADDRESS_TYPE_CODE_MISC;
    case 1:
    case "ADDRESS_TYPE_CODE_HOME":
      return AddressTypeCode.ADDRESS_TYPE_CODE_HOME;
    case 2:
    case "ADDRESS_TYPE_CODE_BIZZ":
      return AddressTypeCode.ADDRESS_TYPE_CODE_BIZZ;
    case 3:
    case "ADDRESS_TYPE_CODE_GEOG":
      return AddressTypeCode.ADDRESS_TYPE_CODE_GEOG;
    case -1:
    case "UNRECOGNIZED":
    default:
      return AddressTypeCode.UNRECOGNIZED;
  }
}

export function addressTypeCodeToJSON(object: AddressTypeCode): string {
  switch (object) {
    case AddressTypeCode.ADDRESS_TYPE_CODE_MISC:
      return "ADDRESS_TYPE_CODE_MISC";
    case AddressTypeCode.ADDRESS_TYPE_CODE_HOME:
      return "ADDRESS_TYPE_CODE_HOME";
    case AddressTypeCode.ADDRESS_TYPE_CODE_BIZZ:
      return "ADDRESS_TYPE_CODE_BIZZ";
    case AddressTypeCode.ADDRESS_TYPE_CODE_GEOG:
      return "ADDRESS_TYPE_CODE_GEOG";
    case AddressTypeCode.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

/**
 * Definition: Identifies the national identification type.
 * NationalIdentifierTypeCode applies a restriction over the codes present in ISO20022
 * datatype ‘TypeOfIdentification4Code’.
 */
export enum NationalIdentifierTypeCode {
  /**
   * NATIONAL_IDENTIFIER_TYPE_CODE_MISC - Unspecified
   * A national identifier which may be known but which cannot otherwise be
   * categorized or the category of which the sender is unable to determine.
   */
  NATIONAL_IDENTIFIER_TYPE_CODE_MISC = 0,
  /**
   * NATIONAL_IDENTIFIER_TYPE_CODE_ARNU - Alien registration number
   * Number assigned by a government agency to identify foreign nationals.
   */
  NATIONAL_IDENTIFIER_TYPE_CODE_ARNU = 1,
  /**
   * NATIONAL_IDENTIFIER_TYPE_CODE_CCPT - Passport number
   * Number assigned by a passport authority.
   */
  NATIONAL_IDENTIFIER_TYPE_CODE_CCPT = 2,
  /**
   * NATIONAL_IDENTIFIER_TYPE_CODE_RAID - Registration authority identifier
   * Identifier of a legal entity as maintained by a registration authority.
   */
  NATIONAL_IDENTIFIER_TYPE_CODE_RAID = 3,
  /**
   * NATIONAL_IDENTIFIER_TYPE_CODE_DRLC - Driver license number
   * Number assigned to a driver's license.
   */
  NATIONAL_IDENTIFIER_TYPE_CODE_DRLC = 4,
  /**
   * NATIONAL_IDENTIFIER_TYPE_CODE_FIIN - Foreign investment identity number
   * Number assigned to a foreign investor (other than the alien number).
   */
  NATIONAL_IDENTIFIER_TYPE_CODE_FIIN = 5,
  /**
   * NATIONAL_IDENTIFIER_TYPE_CODE_TXID - Tax identification number
   * Number assigned by a tax authority to an entity.
   */
  NATIONAL_IDENTIFIER_TYPE_CODE_TXID = 6,
  /**
   * NATIONAL_IDENTIFIER_TYPE_CODE_SOCS - Social security number
   * Number assigned by a social security agency.
   */
  NATIONAL_IDENTIFIER_TYPE_CODE_SOCS = 7,
  /**
   * NATIONAL_IDENTIFIER_TYPE_CODE_IDCD - Identity card number
   * Number assigned by a national authority to an identity card.
   */
  NATIONAL_IDENTIFIER_TYPE_CODE_IDCD = 8,
  /**
   * NATIONAL_IDENTIFIER_TYPE_CODE_LEIX - Legal Entity Identifier
   * Legal Entity Identifier (LEI) assigned in accordance with ISO 17442.
   * The LEI is a 20-character, alpha-numeric code that enables clear and unique
   * identification of legal entities participating in financial transactions.
   */
  NATIONAL_IDENTIFIER_TYPE_CODE_LEIX = 9,
  UNRECOGNIZED = -1,
}

export function nationalIdentifierTypeCodeFromJSON(object: any): NationalIdentifierTypeCode {
  switch (object) {
    case 0:
    case "NATIONAL_IDENTIFIER_TYPE_CODE_MISC":
      return NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_MISC;
    case 1:
    case "NATIONAL_IDENTIFIER_TYPE_CODE_ARNU":
      return NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_ARNU;
    case 2:
    case "NATIONAL_IDENTIFIER_TYPE_CODE_CCPT":
      return NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_CCPT;
    case 3:
    case "NATIONAL_IDENTIFIER_TYPE_CODE_RAID":
      return NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_RAID;
    case 4:
    case "NATIONAL_IDENTIFIER_TYPE_CODE_DRLC":
      return NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_DRLC;
    case 5:
    case "NATIONAL_IDENTIFIER_TYPE_CODE_FIIN":
      return NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_FIIN;
    case 6:
    case "NATIONAL_IDENTIFIER_TYPE_CODE_TXID":
      return NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_TXID;
    case 7:
    case "NATIONAL_IDENTIFIER_TYPE_CODE_SOCS":
      return NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_SOCS;
    case 8:
    case "NATIONAL_IDENTIFIER_TYPE_CODE_IDCD":
      return NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_IDCD;
    case 9:
    case "NATIONAL_IDENTIFIER_TYPE_CODE_LEIX":
      return NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_LEIX;
    case -1:
    case "UNRECOGNIZED":
    default:
      return NationalIdentifierTypeCode.UNRECOGNIZED;
  }
}

export function nationalIdentifierTypeCodeToJSON(object: NationalIdentifierTypeCode): string {
  switch (object) {
    case NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_MISC:
      return "NATIONAL_IDENTIFIER_TYPE_CODE_MISC";
    case NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_ARNU:
      return "NATIONAL_IDENTIFIER_TYPE_CODE_ARNU";
    case NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_CCPT:
      return "NATIONAL_IDENTIFIER_TYPE_CODE_CCPT";
    case NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_RAID:
      return "NATIONAL_IDENTIFIER_TYPE_CODE_RAID";
    case NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_DRLC:
      return "NATIONAL_IDENTIFIER_TYPE_CODE_DRLC";
    case NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_FIIN:
      return "NATIONAL_IDENTIFIER_TYPE_CODE_FIIN";
    case NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_TXID:
      return "NATIONAL_IDENTIFIER_TYPE_CODE_TXID";
    case NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_SOCS:
      return "NATIONAL_IDENTIFIER_TYPE_CODE_SOCS";
    case NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_IDCD:
      return "NATIONAL_IDENTIFIER_TYPE_CODE_IDCD";
    case NationalIdentifierTypeCode.NATIONAL_IDENTIFIER_TYPE_CODE_LEIX:
      return "NATIONAL_IDENTIFIER_TYPE_CODE_LEIX";
    case NationalIdentifierTypeCode.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

/**
 * Definition: Identifies the national script from which transliteration to Latin
 * script is applied.
 */
export enum TransliterationMethodCode {
  /**
   * TRANSLITERATION_METHOD_CODE_OTHR - Script other than those listed below
   * Unspecified Standard
   */
  TRANSLITERATION_METHOD_CODE_OTHR = 0,
  /**
   * TRANSLITERATION_METHOD_CODE_ARAB - Arabic (Arabic language)
   * ISO 233-2:1993
   */
  TRANSLITERATION_METHOD_CODE_ARAB = 1,
  /**
   * TRANSLITERATION_METHOD_CODE_ARAN - Arabic (Persian language)
   * ISO 233-3:1999
   */
  TRANSLITERATION_METHOD_CODE_ARAN = 2,
  /**
   * TRANSLITERATION_METHOD_CODE_ARMN - Armenian
   * ISO 9985:1996
   */
  TRANSLITERATION_METHOD_CODE_ARMN = 3,
  /**
   * TRANSLITERATION_METHOD_CODE_CYRL - Cyrillic
   * ISO 9:1995
   */
  TRANSLITERATION_METHOD_CODE_CYRL = 4,
  /**
   * TRANSLITERATION_METHOD_CODE_DEVA - Devanagari & related Indic
   * ISO 15919:2001
   */
  TRANSLITERATION_METHOD_CODE_DEVA = 5,
  /**
   * TRANSLITERATION_METHOD_CODE_GEOR - Georgian
   * ISO 9984:1996
   */
  TRANSLITERATION_METHOD_CODE_GEOR = 6,
  /**
   * TRANSLITERATION_METHOD_CODE_GREK - Greek
   * ISO 843:1997
   */
  TRANSLITERATION_METHOD_CODE_GREK = 7,
  /**
   * TRANSLITERATION_METHOD_CODE_HANI - Han (Hanzi, Kanji, Hanja)
   * ISO 7098:2015
   */
  TRANSLITERATION_METHOD_CODE_HANI = 8,
  /**
   * TRANSLITERATION_METHOD_CODE_HEBR - Hebrew
   * ISO 259-2:1994
   */
  TRANSLITERATION_METHOD_CODE_HEBR = 9,
  /**
   * TRANSLITERATION_METHOD_CODE_KANA - Kana
   * ISO 3602:1989
   */
  TRANSLITERATION_METHOD_CODE_KANA = 10,
  /**
   * TRANSLITERATION_METHOD_CODE_KORE - Korean
   * Revised Romanization of Korean
   */
  TRANSLITERATION_METHOD_CODE_KORE = 11,
  /**
   * TRANSLITERATION_METHOD_CODE_THAI - Thai
   * ISO 11940-2:2007
   */
  TRANSLITERATION_METHOD_CODE_THAI = 12,
  UNRECOGNIZED = -1,
}

export function transliterationMethodCodeFromJSON(object: any): TransliterationMethodCode {
  switch (object) {
    case 0:
    case "TRANSLITERATION_METHOD_CODE_OTHR":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_OTHR;
    case 1:
    case "TRANSLITERATION_METHOD_CODE_ARAB":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_ARAB;
    case 2:
    case "TRANSLITERATION_METHOD_CODE_ARAN":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_ARAN;
    case 3:
    case "TRANSLITERATION_METHOD_CODE_ARMN":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_ARMN;
    case 4:
    case "TRANSLITERATION_METHOD_CODE_CYRL":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_CYRL;
    case 5:
    case "TRANSLITERATION_METHOD_CODE_DEVA":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_DEVA;
    case 6:
    case "TRANSLITERATION_METHOD_CODE_GEOR":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_GEOR;
    case 7:
    case "TRANSLITERATION_METHOD_CODE_GREK":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_GREK;
    case 8:
    case "TRANSLITERATION_METHOD_CODE_HANI":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_HANI;
    case 9:
    case "TRANSLITERATION_METHOD_CODE_HEBR":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_HEBR;
    case 10:
    case "TRANSLITERATION_METHOD_CODE_KANA":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_KANA;
    case 11:
    case "TRANSLITERATION_METHOD_CODE_KORE":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_KORE;
    case 12:
    case "TRANSLITERATION_METHOD_CODE_THAI":
      return TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_THAI;
    case -1:
    case "UNRECOGNIZED":
    default:
      return TransliterationMethodCode.UNRECOGNIZED;
  }
}

export function transliterationMethodCodeToJSON(object: TransliterationMethodCode): string {
  switch (object) {
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_OTHR:
      return "TRANSLITERATION_METHOD_CODE_OTHR";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_ARAB:
      return "TRANSLITERATION_METHOD_CODE_ARAB";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_ARAN:
      return "TRANSLITERATION_METHOD_CODE_ARAN";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_ARMN:
      return "TRANSLITERATION_METHOD_CODE_ARMN";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_CYRL:
      return "TRANSLITERATION_METHOD_CODE_CYRL";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_DEVA:
      return "TRANSLITERATION_METHOD_CODE_DEVA";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_GEOR:
      return "TRANSLITERATION_METHOD_CODE_GEOR";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_GREK:
      return "TRANSLITERATION_METHOD_CODE_GREK";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_HANI:
      return "TRANSLITERATION_METHOD_CODE_HANI";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_HEBR:
      return "TRANSLITERATION_METHOD_CODE_HEBR";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_KANA:
      return "TRANSLITERATION_METHOD_CODE_KANA";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_KORE:
      return "TRANSLITERATION_METHOD_CODE_KORE";
    case TransliterationMethodCode.TRANSLITERATION_METHOD_CODE_THAI:
      return "TRANSLITERATION_METHOD_CODE_THAI";
    case TransliterationMethodCode.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}
