/* eslint-disable */
import _m0 from "protobufjs/minimal";
import {
  AddressTypeCode,
  addressTypeCodeFromJSON,
  addressTypeCodeToJSON,
  LegalPersonNameTypeCode,
  legalPersonNameTypeCodeFromJSON,
  legalPersonNameTypeCodeToJSON,
  NationalIdentifierTypeCode,
  nationalIdentifierTypeCodeFromJSON,
  nationalIdentifierTypeCodeToJSON,
  NaturalPersonNameTypeCode,
  naturalPersonNameTypeCodeFromJSON,
  naturalPersonNameTypeCodeToJSON,
} from "./enum";

export const protobufPackage = "ivms101";

export interface Person {
  /** Definition: a uniquely distinguishable individual; one single person. */
  naturalPerson?:
    | NaturalPerson
    | undefined;
  /**
   * Definition: any entity other than a natural person that can establish a
   * permanent customer relationship with an affected entity or otherwise own
   * property. This can include companies, bodies corporate, foundations, anstalt,
   * partnerships, or associations and other relevantly similar entities.
   */
  legalPerson?: LegalPerson | undefined;
}

/** Definition: refers to a uniquely distinguishable individual; one single person */
export interface NaturalPerson {
  /**
   * Definition: the distinct words used as identification for an individual.
   * Required
   */
  name:
    | NaturalPersonName
    | undefined;
  /**
   * Definition: the particulars of a location at which a person may be communicated with.
   * Zero or More
   */
  geographicAddresses: Address[];
  /**
   * Definition: a distinct identifier used by governments of countries to uniquely
   * identify a natural or legal person.
   * Optional
   */
  nationalIdentification:
    | NationalIdentification
    | undefined;
  /**
   * Definition: a distinct identifier that uniquely identifies the person to the
   * institution in context.
   * Datatype: “Max50Text”
   * Optional
   */
  customerIdentification: string;
  /**
   * Definition: date and place of birth of a person.
   * Optional
   */
  dateAndPlaceOfBirth:
    | DateAndPlaceOfBirth
    | undefined;
  /**
   * Definition: country in which a person resides (the place of a person's home).
   * The value used for the field country must be present on the ISO-3166-1 alpha-2
   * codes or the value XX.
   * Datatype: “CountryCode”
   * Optional
   */
  countryOfResidence: string;
}

export interface NaturalPersonName {
  /**
   * At least one occurrence of naturalPersonNameID must have the value ‘LEGL’
   * specified in the element naturalPersonNameIdentifierType.
   * Definition: full name separated into primary and secondary identifier.
   * One or more
   */
  nameIdentifiers: NaturalPersonNameId[];
  /**
   * Definition: full name separated into primary and secondary identifier using
   * local characters.
   * Zero or more
   */
  localNameIdentifiers: LocalNaturalPersonNameId[];
  /**
   * Definition: Alternate representation of a name that corresponds to the manner
   * the name is pronounced.
   * Zero or more
   */
  phoneticNameIdentifiers: LocalNaturalPersonNameId[];
}

export interface NaturalPersonNameId {
  /**
   * Definition: This may be the family name, the maiden name or the married name,
   * the main name, the surname, and in some cases, the entire name where the natural
   * person’s name cannot be divided into two parts, or where the sender is unable to
   * divide the natural person’s name into two parts.
   * Datatype: “Max100Text”
   * Required
   */
  primaryIdentifier: string;
  /**
   * Definition: These may be the forenames, familiar names, given names, initials,
   * prefixes, suffixes or Roman numerals (where considered to be legally part of the
   * name) or any other secondary names.
   * Datatype: “Max100Text”
   * Optional
   */
  secondaryIdentifier: string;
  /**
   * Definition: The nature of the name specified.
   * Required
   */
  nameIdentifierType: NaturalPersonNameTypeCode;
}

export interface LocalNaturalPersonNameId {
  /**
   * Definition: This may be the family name, the maiden name or the married name,
   * the main name, the surname, and in some cases, the entire name where the natural
   * person’s name cannot be divided into two parts, or where the sender is unable to
   * divide the natural person’s name into two parts.
   * Datatype: “LocalMax100Text”
   * Required
   */
  primaryIdentifier: string;
  /**
   * Definition: These may be the forenames, familiar names, given names, initials,
   * prefixes, suffixes or Roman numerals (where considered to be legally part of
   * the name) or any other secondary names.
   * Datatype: “LocalMax100Text”
   * Optional
   */
  secondaryIdentifier: string;
  /**
   * Definition: The nature of the name specified.
   * Required
   */
  nameIdentifierType: NaturalPersonNameTypeCode;
}

/**
 * Constraint: ValidAddress
 * There must be at least one occurrence of the element addressLine or (streetName and
 * buildingName and/or buildingNumber).
 */
export interface Address {
  /**
   * Definition: Identifies the nature of the address.
   * Required
   */
  addressType: AddressTypeCode;
  /**
   * Definition: Identification of a division of a large organisation or building.
   * Datatype: “Max50Text”
   * Optional
   */
  department: string;
  /**
   * Definition: Identification of a sub-division of a large organisation or building.
   * Datatype: “Max70Text”
   * Optional
   */
  subDepartment: string;
  /**
   * Definition: Name of a street or thoroughfare.
   * Datatype: “Max70Text”
   * Optional
   */
  streetName: string;
  /**
   * Definition: Number that identifies the position of a building on a street.
   * Datatype: “Max16Text”
   * Optional
   */
  buildingNumber: string;
  /**
   * Definition: Name of the building or house.
   * Datatype: “Max35Text”
   * Optional
   */
  buildingName: string;
  /**
   * Definition: Floor or storey within a building.
   * Datatype: “Max70Text”
   * Optional
   */
  floor: string;
  /**
   * Definition: Numbered box in a post office, assigned to a person or organisation,
   * where letters are kept until called for.
   * Datatype: “Max16Text”
   * Optional
   */
  postBox: string;
  /**
   * Definition: Building room number.
   * Datatype: “Max70Text”
   * Optional
   */
  room: string;
  /**
   * Definition: Identifier consisting of a group of letters and/or numbers that is
   * added to a postal address to assist the sorting of mail.
   * Datatype: “Max16Text”
   * Optional
   */
  postCode: string;
  /**
   * Definition: Name of a built-up area, with defined boundaries and a local government.
   * Datatype: “Max35Text”
   * Optional
   */
  townName: string;
  /**
   * Definition: Specific location name within the town.
   * Datatype: “Max35Text”
   * Optional
   */
  townLocationName: string;
  /**
   * Definition: Identifies a subdivision within a country subdivision.
   * Datatype: “Max35Text”
   * Optional
   */
  districtName: string;
  /**
   * Definition: Identifies a subdivision of a country for example, state, region,
   * province, départment or county.
   * Datatype: “Max35Text”
   * Optional
   */
  countrySubDivision: string;
  /**
   * Definition: Information that locates and identifies a specific address, as
   * defined by postal services, presented in free format text.
   * Datatype: “Max70Text”
   * Zero to Seven
   */
  addressLine: string[];
  /**
   * Constraint: The value used for the field country must be present on the
   * ISO-3166-1 alpha-2 codes or the value XX.
   * Datatype: “CountryCode”
   * Required
   */
  country: string;
}

/**
 * Constraint: DateInPast
 * If dateOfBirth is specified, the date specified must be a historic date (i.e. a date
 * prior to the current date)
 */
export interface DateAndPlaceOfBirth {
  /**
   * Definition: Date on which a person is born.
   * Definition: A point in time, represented as a day within the calendar year.
   * Compliant with ISO 8601.
   * Type: Text
   * Format: YYYY-MM-DD
   * Regex: ^([0-9]{4})-([0-9]{2})-([0-9]{2})$
   * Required
   */
  dateOfBirth: string;
  /**
   * Definition: The town and/or the city and/or the suburb and/or the country
   * subdivision and/or the country where the person was born.
   * Datatype: “Max70Text”
   * Required
   */
  placeOfBirth: string;
}

/**
 * Constraint: ValidNationalIdentifierLegalPerson
 * A legal person must have a value for nationalIdentifierType of either ‘RAID’ or
 * ‘MISC’ or ‘LEIX’ or ‘TXID’.
 * Constraint: CompleteNationalIdentifierLegalPerson
 * A LegalPerson must not have a value for countryOfIssue and must have a value for the
 * element RegistrationAuthority if the value for nationalIdentifierType is not ‘LEIX’
 * Constraint: ValidLEI
 * A LegalPerson with a nationalIdentifierType of ‘LEIX’ must have a value for the
 * element nationalIdentifier that adheres to the convention as stated in datatype
 * ‘LEIText’.
 */
export interface NationalIdentification {
  /**
   * Definition: An identifier issued by an appropriate issuing authority.
   * Constraint: ValidLEI
   * Datatype: “Max35Text”
   * Required
   */
  nationalIdentifier: string;
  /**
   * Definition: Specifies the type of identifier specified.
   * Required
   */
  nationalIdentifierType: NationalIdentifierTypeCode;
  /**
   * Definition: Country of the issuing authority.
   * Datatype: “CountryCode”
   * Optional
   */
  countryOfIssue: string;
  /**
   * Definition: A code specifying the registration authority.
   * Constraint: The value used for the applicable element must be present on the
   * GLEIF Registration Authorities List.
   * Datatype: “RegistrationAuthority”
   * Optional
   */
  registrationAuthority: string;
}

/**
 * Definition: refers to any entity other than a natural person that can establish a
 * permanent customer relationship with an affected entity or otherwise own property.
 * This can include companies, bodies corporate, foundations, anstalt, partnerships, or
 * associations and other relevantly similar entities.
 * Constraint: OriginatorInformationLegalPerson
 * If the originator is a LegalPerson either geographicAddress (with an addressType
 * value of ‘GEOG’) and/or nationalIdentification and/or customerNumber is required.
 */
export interface LegalPerson {
  /**
   * Definition: The name of the legal person.
   * Constraint: LegalNamePresentLegalPerson
   * At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’
   * specified in the element legalPersonNameIdentifierType.
   */
  name:
    | LegalPersonName
    | undefined;
  /**
   * Definition: The address of the legal person.
   * Zero or more
   */
  geographicAddresses: Address[];
  /**
   * Definition: The unique identification number applied by the VASP to customer.
   * NOTE The specification has a descrepency in that 5.2.9.3.3 specifies an element
   * name as "customerNumber", while the table in 5.2.9.1 calls that element
   * "customerIdentification"
   * Datatype: “Max50Text”
   * Optional
   */
  customerNumber: string;
  /**
   * Definition: A distinct identifier used by governments of countries to uniquely
   * identify a natural or legal person.
   * Optional
   */
  nationalIdentification:
    | NationalIdentification
    | undefined;
  /**
   * Definition: The country in which the legal person is registered.
   * Constraint: The value used for the field country must be present on the
   * ISO-3166-1 alpha-2 codes or the value XX.
   * Datatype: “CountryCode”
   * Optional
   */
  countryOfRegistration: string;
}

export interface LegalPersonName {
  /**
   * Definition: The name and type of name by which the legal person is known.
   * Constraint: LegalNamePresent
   * At least one occurrence of legalPersonNameIdentifier must have the value
   * ‘LEGL’ specified in the element legalPersonNameIdentifierType.
   * One or more
   */
  nameIdentifiers: LegalPersonNameId[];
  /**
   * Definition: The name and type of name by which the legal person is known using
   * local characters.
   * Zero or more
   */
  localNameIdentifiers: LocalLegalPersonNameId[];
  /**
   * Definition: The name and type of name by which the legal person is known using
   * local characters.
   * Zero or more
   */
  phoneticNameIdentifiers: LocalLegalPersonNameId[];
}

export interface LegalPersonNameId {
  /**
   * Definition: Name by which the legal person is known.
   * Datatype: “Max100Text”
   * Required
   */
  legalPersonName: string;
  /**
   * Definition: The nature of the name specified.
   * Required
   */
  legalPersonNameIdentifierType: LegalPersonNameTypeCode;
}

export interface LocalLegalPersonNameId {
  /**
   * Definition: Name by which the legal person is known.
   * Datatype: "LocalMax100Text"
   * Required
   */
  legalPersonName: string;
  /**
   * Definition: The nature of the name specified.
   * Required
   */
  legalPersonNameIdentifierType: LegalPersonNameTypeCode;
}

function createBasePerson(): Person {
  return { naturalPerson: undefined, legalPerson: undefined };
}

export const Person = {
  encode(message: Person, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.naturalPerson !== undefined) {
      NaturalPerson.encode(message.naturalPerson, writer.uint32(10).fork()).ldelim();
    }
    if (message.legalPerson !== undefined) {
      LegalPerson.encode(message.legalPerson, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Person {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasePerson();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.naturalPerson = NaturalPerson.decode(reader, reader.uint32());
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.legalPerson = LegalPerson.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Person {
    return {
      naturalPerson: isSet(object.naturalPerson) ? NaturalPerson.fromJSON(object.naturalPerson) : undefined,
      legalPerson: isSet(object.legalPerson) ? LegalPerson.fromJSON(object.legalPerson) : undefined,
    };
  },

  toJSON(message: Person): unknown {
    const obj: any = {};
    message.naturalPerson !== undefined &&
      (obj.naturalPerson = message.naturalPerson ? NaturalPerson.toJSON(message.naturalPerson) : undefined);
    message.legalPerson !== undefined &&
      (obj.legalPerson = message.legalPerson ? LegalPerson.toJSON(message.legalPerson) : undefined);
    return obj;
  },

  create<I extends Exact<DeepPartial<Person>, I>>(base?: I): Person {
    return Person.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Person>, I>>(object: I): Person {
    const message = createBasePerson();
    message.naturalPerson = (object.naturalPerson !== undefined && object.naturalPerson !== null)
      ? NaturalPerson.fromPartial(object.naturalPerson)
      : undefined;
    message.legalPerson = (object.legalPerson !== undefined && object.legalPerson !== null)
      ? LegalPerson.fromPartial(object.legalPerson)
      : undefined;
    return message;
  },
};

function createBaseNaturalPerson(): NaturalPerson {
  return {
    name: undefined,
    geographicAddresses: [],
    nationalIdentification: undefined,
    customerIdentification: "",
    dateAndPlaceOfBirth: undefined,
    countryOfResidence: "",
  };
}

export const NaturalPerson = {
  encode(message: NaturalPerson, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.name !== undefined) {
      NaturalPersonName.encode(message.name, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.geographicAddresses) {
      Address.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    if (message.nationalIdentification !== undefined) {
      NationalIdentification.encode(message.nationalIdentification, writer.uint32(26).fork()).ldelim();
    }
    if (message.customerIdentification !== "") {
      writer.uint32(34).string(message.customerIdentification);
    }
    if (message.dateAndPlaceOfBirth !== undefined) {
      DateAndPlaceOfBirth.encode(message.dateAndPlaceOfBirth, writer.uint32(42).fork()).ldelim();
    }
    if (message.countryOfResidence !== "") {
      writer.uint32(50).string(message.countryOfResidence);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NaturalPerson {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNaturalPerson();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.name = NaturalPersonName.decode(reader, reader.uint32());
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.geographicAddresses.push(Address.decode(reader, reader.uint32()));
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.nationalIdentification = NationalIdentification.decode(reader, reader.uint32());
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.customerIdentification = reader.string();
          continue;
        case 5:
          if (tag != 42) {
            break;
          }

          message.dateAndPlaceOfBirth = DateAndPlaceOfBirth.decode(reader, reader.uint32());
          continue;
        case 6:
          if (tag != 50) {
            break;
          }

          message.countryOfResidence = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NaturalPerson {
    return {
      name: isSet(object.name) ? NaturalPersonName.fromJSON(object.name) : undefined,
      geographicAddresses: Array.isArray(object?.geographicAddress)
        ? object.geographicAddress.map((e: any) => Address.fromJSON(e))
        : [],
      nationalIdentification: isSet(object.nationalIdentification)
        ? NationalIdentification.fromJSON(object.nationalIdentification)
        : undefined,
      customerIdentification: isSet(object.customerIdentification) ? String(object.customerIdentification) : "",
      dateAndPlaceOfBirth: isSet(object.dateAndPlaceOfBirth)
        ? DateAndPlaceOfBirth.fromJSON(object.dateAndPlaceOfBirth)
        : undefined,
      countryOfResidence: isSet(object.countryOfResidence) ? String(object.countryOfResidence) : "",
    };
  },

  toJSON(message: NaturalPerson): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name ? NaturalPersonName.toJSON(message.name) : undefined);
    if (message.geographicAddresses) {
      obj.geographicAddress = message.geographicAddresses.map((e) => e ? Address.toJSON(e) : undefined);
    } else {
      obj.geographicAddress = [];
    }
    message.nationalIdentification !== undefined && (obj.nationalIdentification = message.nationalIdentification
      ? NationalIdentification.toJSON(message.nationalIdentification)
      : undefined);
    message.customerIdentification !== undefined && (obj.customerIdentification = message.customerIdentification);
    message.dateAndPlaceOfBirth !== undefined && (obj.dateAndPlaceOfBirth = message.dateAndPlaceOfBirth
      ? DateAndPlaceOfBirth.toJSON(message.dateAndPlaceOfBirth)
      : undefined);
    message.countryOfResidence !== undefined && (obj.countryOfResidence = message.countryOfResidence);
    return obj;
  },

  create<I extends Exact<DeepPartial<NaturalPerson>, I>>(base?: I): NaturalPerson {
    return NaturalPerson.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NaturalPerson>, I>>(object: I): NaturalPerson {
    const message = createBaseNaturalPerson();
    message.name = (object.name !== undefined && object.name !== null)
      ? NaturalPersonName.fromPartial(object.name)
      : undefined;
    message.geographicAddresses = object.geographicAddresses?.map((e) => Address.fromPartial(e)) || [];
    message.nationalIdentification =
      (object.nationalIdentification !== undefined && object.nationalIdentification !== null)
        ? NationalIdentification.fromPartial(object.nationalIdentification)
        : undefined;
    message.customerIdentification = object.customerIdentification ?? "";
    message.dateAndPlaceOfBirth = (object.dateAndPlaceOfBirth !== undefined && object.dateAndPlaceOfBirth !== null)
      ? DateAndPlaceOfBirth.fromPartial(object.dateAndPlaceOfBirth)
      : undefined;
    message.countryOfResidence = object.countryOfResidence ?? "";
    return message;
  },
};

function createBaseNaturalPersonName(): NaturalPersonName {
  return { nameIdentifiers: [], localNameIdentifiers: [], phoneticNameIdentifiers: [] };
}

export const NaturalPersonName = {
  encode(message: NaturalPersonName, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.nameIdentifiers) {
      NaturalPersonNameId.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.localNameIdentifiers) {
      LocalNaturalPersonNameId.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.phoneticNameIdentifiers) {
      LocalNaturalPersonNameId.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NaturalPersonName {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNaturalPersonName();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.nameIdentifiers.push(NaturalPersonNameId.decode(reader, reader.uint32()));
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.localNameIdentifiers.push(LocalNaturalPersonNameId.decode(reader, reader.uint32()));
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.phoneticNameIdentifiers.push(LocalNaturalPersonNameId.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NaturalPersonName {
    return {
      nameIdentifiers: Array.isArray(object?.nameIdentifier)
        ? object.nameIdentifier.map((e: any) => NaturalPersonNameId.fromJSON(e))
        : [],
      localNameIdentifiers: Array.isArray(object?.localNameIdentifier)
        ? object.localNameIdentifier.map((e: any) => LocalNaturalPersonNameId.fromJSON(e))
        : [],
      phoneticNameIdentifiers: Array.isArray(object?.phoneticNameIdentifier)
        ? object.phoneticNameIdentifier.map((e: any) => LocalNaturalPersonNameId.fromJSON(e))
        : [],
    };
  },

  toJSON(message: NaturalPersonName): unknown {
    const obj: any = {};
    if (message.nameIdentifiers) {
      obj.nameIdentifier = message.nameIdentifiers.map((e) => e ? NaturalPersonNameId.toJSON(e) : undefined);
    } else {
      obj.nameIdentifier = [];
    }
    if (message.localNameIdentifiers) {
      obj.localNameIdentifier = message.localNameIdentifiers.map((e) =>
        e ? LocalNaturalPersonNameId.toJSON(e) : undefined
      );
    } else {
      obj.localNameIdentifier = [];
    }
    if (message.phoneticNameIdentifiers) {
      obj.phoneticNameIdentifier = message.phoneticNameIdentifiers.map((e) =>
        e ? LocalNaturalPersonNameId.toJSON(e) : undefined
      );
    } else {
      obj.phoneticNameIdentifier = [];
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NaturalPersonName>, I>>(base?: I): NaturalPersonName {
    return NaturalPersonName.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NaturalPersonName>, I>>(object: I): NaturalPersonName {
    const message = createBaseNaturalPersonName();
    message.nameIdentifiers = object.nameIdentifiers?.map((e) => NaturalPersonNameId.fromPartial(e)) || [];
    message.localNameIdentifiers = object.localNameIdentifiers?.map((e) => LocalNaturalPersonNameId.fromPartial(e)) ||
      [];
    message.phoneticNameIdentifiers =
      object.phoneticNameIdentifiers?.map((e) => LocalNaturalPersonNameId.fromPartial(e)) || [];
    return message;
  },
};

function createBaseNaturalPersonNameId(): NaturalPersonNameId {
  return { primaryIdentifier: "", secondaryIdentifier: "", nameIdentifierType: 0 };
}

export const NaturalPersonNameId = {
  encode(message: NaturalPersonNameId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.primaryIdentifier !== "") {
      writer.uint32(10).string(message.primaryIdentifier);
    }
    if (message.secondaryIdentifier !== "") {
      writer.uint32(18).string(message.secondaryIdentifier);
    }
    if (message.nameIdentifierType !== 0) {
      writer.uint32(24).int32(message.nameIdentifierType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NaturalPersonNameId {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNaturalPersonNameId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.primaryIdentifier = reader.string();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.secondaryIdentifier = reader.string();
          continue;
        case 3:
          if (tag != 24) {
            break;
          }

          message.nameIdentifierType = reader.int32() as any;
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NaturalPersonNameId {
    return {
      primaryIdentifier: isSet(object.primaryIdentifier) ? String(object.primaryIdentifier) : "",
      secondaryIdentifier: isSet(object.secondaryIdentifier) ? String(object.secondaryIdentifier) : "",
      nameIdentifierType: isSet(object.nameIdentifierType)
        ? naturalPersonNameTypeCodeFromJSON(object.nameIdentifierType)
        : 0,
    };
  },

  toJSON(message: NaturalPersonNameId): unknown {
    const obj: any = {};
    message.primaryIdentifier !== undefined && (obj.primaryIdentifier = message.primaryIdentifier);
    message.secondaryIdentifier !== undefined && (obj.secondaryIdentifier = message.secondaryIdentifier);
    message.nameIdentifierType !== undefined &&
      (obj.nameIdentifierType = naturalPersonNameTypeCodeToJSON(message.nameIdentifierType));
    return obj;
  },

  create<I extends Exact<DeepPartial<NaturalPersonNameId>, I>>(base?: I): NaturalPersonNameId {
    return NaturalPersonNameId.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NaturalPersonNameId>, I>>(object: I): NaturalPersonNameId {
    const message = createBaseNaturalPersonNameId();
    message.primaryIdentifier = object.primaryIdentifier ?? "";
    message.secondaryIdentifier = object.secondaryIdentifier ?? "";
    message.nameIdentifierType = object.nameIdentifierType ?? 0;
    return message;
  },
};

function createBaseLocalNaturalPersonNameId(): LocalNaturalPersonNameId {
  return { primaryIdentifier: "", secondaryIdentifier: "", nameIdentifierType: 0 };
}

export const LocalNaturalPersonNameId = {
  encode(message: LocalNaturalPersonNameId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.primaryIdentifier !== "") {
      writer.uint32(10).string(message.primaryIdentifier);
    }
    if (message.secondaryIdentifier !== "") {
      writer.uint32(18).string(message.secondaryIdentifier);
    }
    if (message.nameIdentifierType !== 0) {
      writer.uint32(24).int32(message.nameIdentifierType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LocalNaturalPersonNameId {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLocalNaturalPersonNameId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.primaryIdentifier = reader.string();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.secondaryIdentifier = reader.string();
          continue;
        case 3:
          if (tag != 24) {
            break;
          }

          message.nameIdentifierType = reader.int32() as any;
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): LocalNaturalPersonNameId {
    return {
      primaryIdentifier: isSet(object.primaryIdentifier) ? String(object.primaryIdentifier) : "",
      secondaryIdentifier: isSet(object.secondaryIdentifier) ? String(object.secondaryIdentifier) : "",
      nameIdentifierType: isSet(object.nameIdentifierType)
        ? naturalPersonNameTypeCodeFromJSON(object.nameIdentifierType)
        : 0,
    };
  },

  toJSON(message: LocalNaturalPersonNameId): unknown {
    const obj: any = {};
    message.primaryIdentifier !== undefined && (obj.primaryIdentifier = message.primaryIdentifier);
    message.secondaryIdentifier !== undefined && (obj.secondaryIdentifier = message.secondaryIdentifier);
    message.nameIdentifierType !== undefined &&
      (obj.nameIdentifierType = naturalPersonNameTypeCodeToJSON(message.nameIdentifierType));
    return obj;
  },

  create<I extends Exact<DeepPartial<LocalNaturalPersonNameId>, I>>(base?: I): LocalNaturalPersonNameId {
    return LocalNaturalPersonNameId.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<LocalNaturalPersonNameId>, I>>(object: I): LocalNaturalPersonNameId {
    const message = createBaseLocalNaturalPersonNameId();
    message.primaryIdentifier = object.primaryIdentifier ?? "";
    message.secondaryIdentifier = object.secondaryIdentifier ?? "";
    message.nameIdentifierType = object.nameIdentifierType ?? 0;
    return message;
  },
};

function createBaseAddress(): Address {
  return {
    addressType: 0,
    department: "",
    subDepartment: "",
    streetName: "",
    buildingNumber: "",
    buildingName: "",
    floor: "",
    postBox: "",
    room: "",
    postCode: "",
    townName: "",
    townLocationName: "",
    districtName: "",
    countrySubDivision: "",
    addressLine: [],
    country: "",
  };
}

export const Address = {
  encode(message: Address, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.addressType !== 0) {
      writer.uint32(8).int32(message.addressType);
    }
    if (message.department !== "") {
      writer.uint32(18).string(message.department);
    }
    if (message.subDepartment !== "") {
      writer.uint32(26).string(message.subDepartment);
    }
    if (message.streetName !== "") {
      writer.uint32(34).string(message.streetName);
    }
    if (message.buildingNumber !== "") {
      writer.uint32(42).string(message.buildingNumber);
    }
    if (message.buildingName !== "") {
      writer.uint32(50).string(message.buildingName);
    }
    if (message.floor !== "") {
      writer.uint32(58).string(message.floor);
    }
    if (message.postBox !== "") {
      writer.uint32(66).string(message.postBox);
    }
    if (message.room !== "") {
      writer.uint32(74).string(message.room);
    }
    if (message.postCode !== "") {
      writer.uint32(82).string(message.postCode);
    }
    if (message.townName !== "") {
      writer.uint32(90).string(message.townName);
    }
    if (message.townLocationName !== "") {
      writer.uint32(98).string(message.townLocationName);
    }
    if (message.districtName !== "") {
      writer.uint32(106).string(message.districtName);
    }
    if (message.countrySubDivision !== "") {
      writer.uint32(114).string(message.countrySubDivision);
    }
    for (const v of message.addressLine) {
      writer.uint32(122).string(v!);
    }
    if (message.country !== "") {
      writer.uint32(130).string(message.country);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Address {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAddress();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.addressType = reader.int32() as any;
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.department = reader.string();
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.subDepartment = reader.string();
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.streetName = reader.string();
          continue;
        case 5:
          if (tag != 42) {
            break;
          }

          message.buildingNumber = reader.string();
          continue;
        case 6:
          if (tag != 50) {
            break;
          }

          message.buildingName = reader.string();
          continue;
        case 7:
          if (tag != 58) {
            break;
          }

          message.floor = reader.string();
          continue;
        case 8:
          if (tag != 66) {
            break;
          }

          message.postBox = reader.string();
          continue;
        case 9:
          if (tag != 74) {
            break;
          }

          message.room = reader.string();
          continue;
        case 10:
          if (tag != 82) {
            break;
          }

          message.postCode = reader.string();
          continue;
        case 11:
          if (tag != 90) {
            break;
          }

          message.townName = reader.string();
          continue;
        case 12:
          if (tag != 98) {
            break;
          }

          message.townLocationName = reader.string();
          continue;
        case 13:
          if (tag != 106) {
            break;
          }

          message.districtName = reader.string();
          continue;
        case 14:
          if (tag != 114) {
            break;
          }

          message.countrySubDivision = reader.string();
          continue;
        case 15:
          if (tag != 122) {
            break;
          }

          message.addressLine.push(reader.string());
          continue;
        case 16:
          if (tag != 130) {
            break;
          }

          message.country = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Address {
    return {
      addressType: isSet(object.addressType) ? addressTypeCodeFromJSON(object.addressType) : 0,
      department: isSet(object.department) ? String(object.department) : "",
      subDepartment: isSet(object.subDepartment) ? String(object.subDepartment) : "",
      streetName: isSet(object.streetName) ? String(object.streetName) : "",
      buildingNumber: isSet(object.buildingNumber) ? String(object.buildingNumber) : "",
      buildingName: isSet(object.buildingName) ? String(object.buildingName) : "",
      floor: isSet(object.floor) ? String(object.floor) : "",
      postBox: isSet(object.postBox) ? String(object.postBox) : "",
      room: isSet(object.room) ? String(object.room) : "",
      postCode: isSet(object.postCode) ? String(object.postCode) : "",
      townName: isSet(object.townName) ? String(object.townName) : "",
      townLocationName: isSet(object.townLocationName) ? String(object.townLocationName) : "",
      districtName: isSet(object.districtName) ? String(object.districtName) : "",
      countrySubDivision: isSet(object.countrySubDivision) ? String(object.countrySubDivision) : "",
      addressLine: Array.isArray(object?.addressLine) ? object.addressLine.map((e: any) => String(e)) : [],
      country: isSet(object.country) ? String(object.country) : "",
    };
  },

  toJSON(message: Address): unknown {
    const obj: any = {};
    message.addressType !== undefined && (obj.addressType = addressTypeCodeToJSON(message.addressType));
    message.department !== undefined && (obj.department = message.department);
    message.subDepartment !== undefined && (obj.subDepartment = message.subDepartment);
    message.streetName !== undefined && (obj.streetName = message.streetName);
    message.buildingNumber !== undefined && (obj.buildingNumber = message.buildingNumber);
    message.buildingName !== undefined && (obj.buildingName = message.buildingName);
    message.floor !== undefined && (obj.floor = message.floor);
    message.postBox !== undefined && (obj.postBox = message.postBox);
    message.room !== undefined && (obj.room = message.room);
    message.postCode !== undefined && (obj.postCode = message.postCode);
    message.townName !== undefined && (obj.townName = message.townName);
    message.townLocationName !== undefined && (obj.townLocationName = message.townLocationName);
    message.districtName !== undefined && (obj.districtName = message.districtName);
    message.countrySubDivision !== undefined && (obj.countrySubDivision = message.countrySubDivision);
    if (message.addressLine) {
      obj.addressLine = message.addressLine.map((e) => e);
    } else {
      obj.addressLine = [];
    }
    message.country !== undefined && (obj.country = message.country);
    return obj;
  },

  create<I extends Exact<DeepPartial<Address>, I>>(base?: I): Address {
    return Address.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Address>, I>>(object: I): Address {
    const message = createBaseAddress();
    message.addressType = object.addressType ?? 0;
    message.department = object.department ?? "";
    message.subDepartment = object.subDepartment ?? "";
    message.streetName = object.streetName ?? "";
    message.buildingNumber = object.buildingNumber ?? "";
    message.buildingName = object.buildingName ?? "";
    message.floor = object.floor ?? "";
    message.postBox = object.postBox ?? "";
    message.room = object.room ?? "";
    message.postCode = object.postCode ?? "";
    message.townName = object.townName ?? "";
    message.townLocationName = object.townLocationName ?? "";
    message.districtName = object.districtName ?? "";
    message.countrySubDivision = object.countrySubDivision ?? "";
    message.addressLine = object.addressLine?.map((e) => e) || [];
    message.country = object.country ?? "";
    return message;
  },
};

function createBaseDateAndPlaceOfBirth(): DateAndPlaceOfBirth {
  return { dateOfBirth: "", placeOfBirth: "" };
}

export const DateAndPlaceOfBirth = {
  encode(message: DateAndPlaceOfBirth, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.dateOfBirth !== "") {
      writer.uint32(10).string(message.dateOfBirth);
    }
    if (message.placeOfBirth !== "") {
      writer.uint32(18).string(message.placeOfBirth);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DateAndPlaceOfBirth {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDateAndPlaceOfBirth();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.dateOfBirth = reader.string();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.placeOfBirth = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): DateAndPlaceOfBirth {
    return {
      dateOfBirth: isSet(object.dateOfBirth) ? String(object.dateOfBirth) : "",
      placeOfBirth: isSet(object.placeOfBirth) ? String(object.placeOfBirth) : "",
    };
  },

  toJSON(message: DateAndPlaceOfBirth): unknown {
    const obj: any = {};
    message.dateOfBirth !== undefined && (obj.dateOfBirth = message.dateOfBirth);
    message.placeOfBirth !== undefined && (obj.placeOfBirth = message.placeOfBirth);
    return obj;
  },

  create<I extends Exact<DeepPartial<DateAndPlaceOfBirth>, I>>(base?: I): DateAndPlaceOfBirth {
    return DateAndPlaceOfBirth.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<DateAndPlaceOfBirth>, I>>(object: I): DateAndPlaceOfBirth {
    const message = createBaseDateAndPlaceOfBirth();
    message.dateOfBirth = object.dateOfBirth ?? "";
    message.placeOfBirth = object.placeOfBirth ?? "";
    return message;
  },
};

function createBaseNationalIdentification(): NationalIdentification {
  return { nationalIdentifier: "", nationalIdentifierType: 0, countryOfIssue: "", registrationAuthority: "" };
}

export const NationalIdentification = {
  encode(message: NationalIdentification, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.nationalIdentifier !== "") {
      writer.uint32(10).string(message.nationalIdentifier);
    }
    if (message.nationalIdentifierType !== 0) {
      writer.uint32(16).int32(message.nationalIdentifierType);
    }
    if (message.countryOfIssue !== "") {
      writer.uint32(26).string(message.countryOfIssue);
    }
    if (message.registrationAuthority !== "") {
      writer.uint32(34).string(message.registrationAuthority);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NationalIdentification {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNationalIdentification();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.nationalIdentifier = reader.string();
          continue;
        case 2:
          if (tag != 16) {
            break;
          }

          message.nationalIdentifierType = reader.int32() as any;
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.countryOfIssue = reader.string();
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.registrationAuthority = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NationalIdentification {
    return {
      nationalIdentifier: isSet(object.nationalIdentifier) ? String(object.nationalIdentifier) : "",
      nationalIdentifierType: isSet(object.nationalIdentifierType)
        ? nationalIdentifierTypeCodeFromJSON(object.nationalIdentifierType)
        : 0,
      countryOfIssue: isSet(object.countryOfIssue) ? String(object.countryOfIssue) : "",
      registrationAuthority: isSet(object.registrationAuthority) ? String(object.registrationAuthority) : "",
    };
  },

  toJSON(message: NationalIdentification): unknown {
    const obj: any = {};
    message.nationalIdentifier !== undefined && (obj.nationalIdentifier = message.nationalIdentifier);
    message.nationalIdentifierType !== undefined &&
      (obj.nationalIdentifierType = nationalIdentifierTypeCodeToJSON(message.nationalIdentifierType));
    message.countryOfIssue !== undefined && (obj.countryOfIssue = message.countryOfIssue);
    message.registrationAuthority !== undefined && (obj.registrationAuthority = message.registrationAuthority);
    return obj;
  },

  create<I extends Exact<DeepPartial<NationalIdentification>, I>>(base?: I): NationalIdentification {
    return NationalIdentification.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NationalIdentification>, I>>(object: I): NationalIdentification {
    const message = createBaseNationalIdentification();
    message.nationalIdentifier = object.nationalIdentifier ?? "";
    message.nationalIdentifierType = object.nationalIdentifierType ?? 0;
    message.countryOfIssue = object.countryOfIssue ?? "";
    message.registrationAuthority = object.registrationAuthority ?? "";
    return message;
  },
};

function createBaseLegalPerson(): LegalPerson {
  return {
    name: undefined,
    geographicAddresses: [],
    customerNumber: "",
    nationalIdentification: undefined,
    countryOfRegistration: "",
  };
}

export const LegalPerson = {
  encode(message: LegalPerson, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.name !== undefined) {
      LegalPersonName.encode(message.name, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.geographicAddresses) {
      Address.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    if (message.customerNumber !== "") {
      writer.uint32(26).string(message.customerNumber);
    }
    if (message.nationalIdentification !== undefined) {
      NationalIdentification.encode(message.nationalIdentification, writer.uint32(34).fork()).ldelim();
    }
    if (message.countryOfRegistration !== "") {
      writer.uint32(42).string(message.countryOfRegistration);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LegalPerson {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLegalPerson();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.name = LegalPersonName.decode(reader, reader.uint32());
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.geographicAddresses.push(Address.decode(reader, reader.uint32()));
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.customerNumber = reader.string();
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.nationalIdentification = NationalIdentification.decode(reader, reader.uint32());
          continue;
        case 5:
          if (tag != 42) {
            break;
          }

          message.countryOfRegistration = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): LegalPerson {
    return {
      name: isSet(object.name) ? LegalPersonName.fromJSON(object.name) : undefined,
      geographicAddresses: Array.isArray(object?.geographicAddress)
        ? object.geographicAddress.map((e: any) => Address.fromJSON(e))
        : [],
      customerNumber: isSet(object.customerNumber) ? String(object.customerNumber) : "",
      nationalIdentification: isSet(object.nationalIdentification)
        ? NationalIdentification.fromJSON(object.nationalIdentification)
        : undefined,
      countryOfRegistration: isSet(object.countryOfRegistration) ? String(object.countryOfRegistration) : "",
    };
  },

  toJSON(message: LegalPerson): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name ? LegalPersonName.toJSON(message.name) : undefined);
    if (message.geographicAddresses) {
      obj.geographicAddress = message.geographicAddresses.map((e) => e ? Address.toJSON(e) : undefined);
    } else {
      obj.geographicAddress = [];
    }
    message.customerNumber !== undefined && (obj.customerNumber = message.customerNumber);
    message.nationalIdentification !== undefined && (obj.nationalIdentification = message.nationalIdentification
      ? NationalIdentification.toJSON(message.nationalIdentification)
      : undefined);
    message.countryOfRegistration !== undefined && (obj.countryOfRegistration = message.countryOfRegistration);
    return obj;
  },

  create<I extends Exact<DeepPartial<LegalPerson>, I>>(base?: I): LegalPerson {
    return LegalPerson.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<LegalPerson>, I>>(object: I): LegalPerson {
    const message = createBaseLegalPerson();
    message.name = (object.name !== undefined && object.name !== null)
      ? LegalPersonName.fromPartial(object.name)
      : undefined;
    message.geographicAddresses = object.geographicAddresses?.map((e) => Address.fromPartial(e)) || [];
    message.customerNumber = object.customerNumber ?? "";
    message.nationalIdentification =
      (object.nationalIdentification !== undefined && object.nationalIdentification !== null)
        ? NationalIdentification.fromPartial(object.nationalIdentification)
        : undefined;
    message.countryOfRegistration = object.countryOfRegistration ?? "";
    return message;
  },
};

function createBaseLegalPersonName(): LegalPersonName {
  return { nameIdentifiers: [], localNameIdentifiers: [], phoneticNameIdentifiers: [] };
}

export const LegalPersonName = {
  encode(message: LegalPersonName, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.nameIdentifiers) {
      LegalPersonNameId.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.localNameIdentifiers) {
      LocalLegalPersonNameId.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.phoneticNameIdentifiers) {
      LocalLegalPersonNameId.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LegalPersonName {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLegalPersonName();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.nameIdentifiers.push(LegalPersonNameId.decode(reader, reader.uint32()));
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.localNameIdentifiers.push(LocalLegalPersonNameId.decode(reader, reader.uint32()));
          continue;
        case 3:
          if (tag != 26) {
            break;
          }

          message.phoneticNameIdentifiers.push(LocalLegalPersonNameId.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): LegalPersonName {
    return {
      nameIdentifiers: Array.isArray(object?.nameIdentifier)
        ? object.nameIdentifier.map((e: any) => LegalPersonNameId.fromJSON(e))
        : [],
      localNameIdentifiers: Array.isArray(object?.localNameIdentifier)
        ? object.localNameIdentifier.map((e: any) => LocalLegalPersonNameId.fromJSON(e))
        : [],
      phoneticNameIdentifiers: Array.isArray(object?.phoneticNameIdentifier)
        ? object.phoneticNameIdentifier.map((e: any) => LocalLegalPersonNameId.fromJSON(e))
        : [],
    };
  },

  toJSON(message: LegalPersonName): unknown {
    const obj: any = {};
    if (message.nameIdentifiers) {
      obj.nameIdentifier = message.nameIdentifiers.map((e) => e ? LegalPersonNameId.toJSON(e) : undefined);
    } else {
      obj.nameIdentifier = [];
    }
    if (message.localNameIdentifiers) {
      obj.localNameIdentifier = message.localNameIdentifiers.map((e) =>
        e ? LocalLegalPersonNameId.toJSON(e) : undefined
      );
    } else {
      obj.localNameIdentifier = [];
    }
    if (message.phoneticNameIdentifiers) {
      obj.phoneticNameIdentifier = message.phoneticNameIdentifiers.map((e) =>
        e ? LocalLegalPersonNameId.toJSON(e) : undefined
      );
    } else {
      obj.phoneticNameIdentifier = [];
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<LegalPersonName>, I>>(base?: I): LegalPersonName {
    return LegalPersonName.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<LegalPersonName>, I>>(object: I): LegalPersonName {
    const message = createBaseLegalPersonName();
    message.nameIdentifiers = object.nameIdentifiers?.map((e) => LegalPersonNameId.fromPartial(e)) || [];
    message.localNameIdentifiers = object.localNameIdentifiers?.map((e) => LocalLegalPersonNameId.fromPartial(e)) || [];
    message.phoneticNameIdentifiers =
      object.phoneticNameIdentifiers?.map((e) => LocalLegalPersonNameId.fromPartial(e)) || [];
    return message;
  },
};

function createBaseLegalPersonNameId(): LegalPersonNameId {
  return { legalPersonName: "", legalPersonNameIdentifierType: 0 };
}

export const LegalPersonNameId = {
  encode(message: LegalPersonNameId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.legalPersonName !== "") {
      writer.uint32(10).string(message.legalPersonName);
    }
    if (message.legalPersonNameIdentifierType !== 0) {
      writer.uint32(16).int32(message.legalPersonNameIdentifierType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LegalPersonNameId {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLegalPersonNameId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.legalPersonName = reader.string();
          continue;
        case 2:
          if (tag != 16) {
            break;
          }

          message.legalPersonNameIdentifierType = reader.int32() as any;
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): LegalPersonNameId {
    return {
      legalPersonName: isSet(object.legalPersonName) ? String(object.legalPersonName) : "",
      legalPersonNameIdentifierType: isSet(object.legalPersonNameIdentifierType)
        ? legalPersonNameTypeCodeFromJSON(object.legalPersonNameIdentifierType)
        : 0,
    };
  },

  toJSON(message: LegalPersonNameId): unknown {
    const obj: any = {};
    message.legalPersonName !== undefined && (obj.legalPersonName = message.legalPersonName);
    message.legalPersonNameIdentifierType !== undefined &&
      (obj.legalPersonNameIdentifierType = legalPersonNameTypeCodeToJSON(message.legalPersonNameIdentifierType));
    return obj;
  },

  create<I extends Exact<DeepPartial<LegalPersonNameId>, I>>(base?: I): LegalPersonNameId {
    return LegalPersonNameId.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<LegalPersonNameId>, I>>(object: I): LegalPersonNameId {
    const message = createBaseLegalPersonNameId();
    message.legalPersonName = object.legalPersonName ?? "";
    message.legalPersonNameIdentifierType = object.legalPersonNameIdentifierType ?? 0;
    return message;
  },
};

function createBaseLocalLegalPersonNameId(): LocalLegalPersonNameId {
  return { legalPersonName: "", legalPersonNameIdentifierType: 0 };
}

export const LocalLegalPersonNameId = {
  encode(message: LocalLegalPersonNameId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.legalPersonName !== "") {
      writer.uint32(10).string(message.legalPersonName);
    }
    if (message.legalPersonNameIdentifierType !== 0) {
      writer.uint32(16).int32(message.legalPersonNameIdentifierType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LocalLegalPersonNameId {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLocalLegalPersonNameId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.legalPersonName = reader.string();
          continue;
        case 2:
          if (tag != 16) {
            break;
          }

          message.legalPersonNameIdentifierType = reader.int32() as any;
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): LocalLegalPersonNameId {
    return {
      legalPersonName: isSet(object.legalPersonName) ? String(object.legalPersonName) : "",
      legalPersonNameIdentifierType: isSet(object.legalPersonNameIdentifierType)
        ? legalPersonNameTypeCodeFromJSON(object.legalPersonNameIdentifierType)
        : 0,
    };
  },

  toJSON(message: LocalLegalPersonNameId): unknown {
    const obj: any = {};
    message.legalPersonName !== undefined && (obj.legalPersonName = message.legalPersonName);
    message.legalPersonNameIdentifierType !== undefined &&
      (obj.legalPersonNameIdentifierType = legalPersonNameTypeCodeToJSON(message.legalPersonNameIdentifierType));
    return obj;
  },

  create<I extends Exact<DeepPartial<LocalLegalPersonNameId>, I>>(base?: I): LocalLegalPersonNameId {
    return LocalLegalPersonNameId.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<LocalLegalPersonNameId>, I>>(object: I): LocalLegalPersonNameId {
    const message = createBaseLocalLegalPersonNameId();
    message.legalPersonName = object.legalPersonName ?? "";
    message.legalPersonNameIdentifierType = object.legalPersonNameIdentifierType ?? 0;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
