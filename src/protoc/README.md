Build using:

brew install protofbuf

yarn add --dev ts-proto

protoc --plugin=protoc-gen-ts_proto=./node_modules/.bin/protoc-gen-ts_proto --ts_proto_out=./src/protoc ./src/proto/trisa/gds/v1beta1/api.proto --ts_proto_opt=outputServices=grpc-js,env=node,esModuleInterop=true --proto_path=./src/proto/
