<div align="center">

<img src="https://trisa.io/wp-content/uploads/2019/12/trisa-logo@2x.jpg" height=50>

<br>

# TRISA NodeJS SDK

[![pipeline status](https://gitlab.com/notabene/open-source/trisa-nodejs-sdk/-/badges/pipeline.svg)](https://gitlab.com/notabene/open-source/trisa-nodejs-sdk/-/commits/master)
[![Latest Release](https://gitlab.com/notabene/open-source/trisa-nodejs-sdk/-/badges/release.svg)](https://gitlab.com/notabene/open-source/trisa-nodejs-sdk/-/releases)

NodeJS compatible library to connect to TRISA (trisa.io) Travel Rule sharing alliance

[Getting started](#getting-started) •
[Installation](#installation) •
[Configuration](#configuration)

</div>

## Getting Started

### Step 1: Install the library

```
npm install @notabene/trisa
```

### Step 2: Initialize the client

```javascript
const { Trisa } = require('@notabene/trisa');

const trisa = new Trisa({
  clientCert: '{PEM-ENCODED-CERT}', 
  clientKey: '{PEM-ENCODED-KEY}', 
  rootCert: '{PEM-ENCODED-CERT}', 
});
```

## Configuration


### Testnet directory

For using the testnet directoy of TRISA please set the `directoryAddress` to the corresponding endpoint

```javascript
const { Trisa } = require('@notabene/trisa');
trisa = new Trisa({
  clientCert: clientCert,
  clientKey: clientKey,
  rootCert: caCert,
  directoryAddress: 'api.trisatest.net:443',
});
```

## API methods

- `trisa.trasfer()` does a Travel Rule transfer to the corrssponding VASP
## Appendix

### Example 1

## [License](LICENSE.md)

BSD 3-Clause © Notabene Inc.
